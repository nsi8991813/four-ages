import pygame


class Craft(pygame.sprite.Sprite):

    def __init__(self, game, recipe, product, scale, co_x, co_y):
        super().__init__()
        self.game = game

        self.recipe = recipe
        self.product = product
        self.image = pygame.image.load(f"assets/{scale}_craft.png")
        self.rect = self.image.get_rect()
        self.rect.x = co_x
        self.rect.y = co_y

        if scale == "potion":
            self.item = 0
            self.level = 0