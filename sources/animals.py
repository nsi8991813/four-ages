import pygame


class Animal(pygame.sprite.Sprite):
    def __init__(self, game, name, pos_x, pos_y):
        super().__init__()
        self.game = game

        self.pos_x = pos_x
        self.pos_y = pos_y

        self.lock = False

        self.moving = False
        self.destination = [0, 0]

        self.name = name
        self.image = pygame.image.load(f"assets/animals/{name}.png")
        self.image = pygame.transform.scale(self.image, (100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = self.pos_x * 128 + 205
        self.rect.y = self.pos_y * 128 - 67
        if self.game.potions[3] >= 1:
            self.game.rotate(self)

        self.time = 0