import pygame
import random


class Pnj(pygame.sprite.Sprite):

    def __init__(self, game, name):
        super().__init__()
        self.game = game

        self.name = name
        self.image = pygame.image.load(f"assets/PNJ/{name}.png")
        self.image = pygame.transform.scale(self.image, (120, 120))
        self.rect = self.image.get_rect()
        self.rect.x = 1050
        self.rect.y = 285
        if self.game.potions[3] >= 1:
            self.game.rotate(self)

        with open(f"crafts/trades/{name}.txt") as file:
            trades = []
            for line in file:
                trades.append(line.split())
            i = random.randint(0, (game.age - 1)*8 - 1)
            self.product = trades[i][0]
            if self.product.isdigit():
                self.product = int(self.product)
            self.recipe = trades[i][1:]
            file.close()

