import pygame


class Item(pygame.sprite.Sprite):

    def __init__(self, game, name, pos_x, pos_y, affected):
        super().__init__()
        self.game = game

        # on récupère les valeurs d'entrée
        self.name = name
        self.pos_x = pos_x
        self.pos_y = pos_y

        self.affected = affected

        # gestion de l'image + position
        if self.name not in self.game.posable_item and self.name[0] not in ["1", "2", "3", "4", "5"]:
            self.image = pygame.image.load(f"assets/items/{self.name}.png")
        else:
            if self.name[0] not in ["1", "2", "3", "4", "5"]:
                self.image = pygame.image.load(f"assets/builds/{self.name}.png")
            else:
                self.image = pygame.image.load(f"assets/builds/{self.name[0]}.png")
        self.image = pygame.transform.scale(self.image, (40, 40))
        self.rect = self.image.get_rect()
        self.rect.x = 273 + 128 * self.pos_x
        self.rect.y = 5 + 128 * self.pos_y
        if self.game.potions[3] >= 1:
            self.game.rotate(self)
        if affected == True:
            self.rect.x -= 68
            self.rect.y -= 87

        # gestion du déplacement
        self.can_move = False

    def add_pickaxe(self, build, on_off):
        if build.name == "cave":
            if on_off:
                for i in range(64):
                    build.item.append("stone")
                for i in range(1):
                    build.item.append("big_stone")
                for i in range(5):
                    build.item.append("gold")
                for i in range(8):
                    build.item.append("silver")
                for i in range(10):
                    build.item.append("coal")
                for i in range(1):
                    build.item.append("diamond")
            else:
                for i in range(64):
                    build.item.remove("stone")
                for i in range(1):
                    build.item.remove("big_stone")
                for i in range(5):
                    build.item.remove("gold")
                for i in range(8):
                    build.item.remove("silver")
                for i in range(1):
                    build.item.remove("diamond")
                for i in range(10):
                    build.item.remove("coal")

    def add_axe(self, build, on_off):
        if build.name == "t5" or build.name == "t4" or build.name == "t3" or build.name == "dead_tree":
            if on_off:
                build.time = 0
                build.max_time = 150
                nb_tree = 0
                for build in self.game.all_builds:
                    if build.name == "t1" or build.name == "t2" or build.name == "t3" or build.name == "t4" or build.name == "t5":
                        if build.special == "":
                            nb_tree += 1
                if nb_tree == 1 and build.name != "dead_tree":
                    self.game.reset_tips("Attention !", "C'est votre dernier arbre !", "")

            else:
                build.time = 0
                if build.name != "dead_tree" and build.name != "t5":
                    build.max_time = int(build.name[1])*90
                elif build.name == "dead_tree":
                    build.max_time = None
                else:
                    build.max_time = 280

            i = 0
            for item in self.game.all_items:
                if item.pos_x == build.pos_x and item.pos_y == build.pos_y and item.affected == False:
                    i += 1
            if i == 3:
                build.max_time = None

    def add_shovel(self, build, on_off):
        if build.name == "tas":
            if on_off:
                build.item.append("oil")
            else:
                build.item.remove("oil")