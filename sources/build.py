import pygame
import random


class Build(pygame.sprite.Sprite):

    def __init__(self, game, name, pos_x, pos_y, first, special):
        super().__init__()
        self.game = game

        self.name = name
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.time = 0
        self.max_time = None
        self.item = []
        self.can_affect = None
        self.affected_object = None
        self.special = special
        self.duration = None
        self.star = 0

        if first: #met les infos spécifiques à chaque batiment. (EX: le fuel dans le feu)
            self.set_stats()
            if self.name == "chest":
                self.chest_item = {}
            elif self.name == "treasure":
                self.items_in = []
            elif self.name == "fire" or self.name == "kitchen" or self.name == "building":
                self.fuel = 0
            elif self.name == "enclos":
                self.food = 0

        if self.special == "":
            self.image = pygame.image.load(f"assets/builds/{self.name}.png")
        else:
            self.image = pygame.image.load(f"assets/builds/growing/{self.name + self.special}.png")
        self.image = pygame.transform.scale(self.image, (100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = self.pos_x * 128 + 205
        self.rect.y = self.pos_y * 128 - 67
        if self.game.potions[3] >= 1:
            self.game.rotate(self)

        self.bar_color1 = [34, 199, 26]
        self.bar_color2 = [236, 28, 36]


    # associe à chaque batiment les infos qui lui sont propres
    def set_stats(self):
        if self.name == "t1" or self.name == "t2" or self.name == "t3" or self.name == "t4":
            self.max_time = int(self.name[1]) * 70
            if self.name == "t3" or self.name == "t4":
                self.can_affect = "axe"

        elif self.name == "s1" or self.name == "s2" or self.name == "s3":
            self.max_time = int(self.name[1]) * 40

        elif self.name == "s4":
            self.max_time = 170
            if self.special == "wheat":
                self.item = ["wheat"]

        elif self.name == "t5":
            self.max_time = 280
            self.can_affect = "axe"
            if self.special == "apple":
                self.duration = 10
                self.item = ["apple", "apple", "branch", "apple"]
            elif self.special == "magic":
                self.duration = 10
                self.item = ["magic", "magic", "magic", "branch"]
            else:
                self.item = ["branch", "branch", "branch", "branch", "branch", "leaves", "leaves", "t1", "t1", "t1", "branch"]

        elif self.name == "dead_tree":
            self.can_affect = "axe"

        elif self.name == "cave":
            self.max_time = 120
            self.item = ["stone"]
            self.can_affect = "pickaxe"

        elif self.name == "fire":
            self.item = ["flame"]

        elif self.name == "enclos":
            self.duration = 7
            
        elif self.name == "tas":
            self.item = ["clay", "clay", "clay", "clay", "clay", "clay", "sand", "sand", "sand"]
            self.max_time = 300
            self.can_affect = "shovel"

        elif self.name == "garden":
            self.item = ["potato", "tomato", "sugar_cane"]
            self.max_time = 320

    #gère l'avancée du batiment avant la production de l'item
    def produce(self):
        self.time += round(1.47**self.star) * self.game.build_speed_potions
        if self.time >= self.max_time:
            self.time = 0
            self.effect()

    #gère la barre de chargement en dessous des batiments
    def update_bar(self, screen):
        bar_pos = [self.rect.x - 10, self.rect.y + 93, self.time * 127 / self.max_time, 10]
        if self.affected_object != "axe" and self.name != "trap":
            pygame.draw.rect(screen, self.bar_color1, bar_pos)
        else:
            pygame.draw.rect(screen, self.bar_color2, bar_pos)

    #fait les effets des batiments
    def effect(self):
        if self.name == "t1" or self.name == "t2" or self.name == "t3" or self.name == "t4":
            self.game.all_builds.remove(self)
            if self.affected_object != "axe":
                self.game.spawn_build(self.pos_x, self.pos_y, f"t{str(int(self.name[1]) + 1)}", self.special)
            else:
                self.game.remove_item("axe", self.pos_x, self.pos_y)
                for i in range(int(self.name[1]) - 2):
                    self.game.spawn_item("planks", self.pos_x, self.pos_y, False)

        elif self.name == "s1" or self.name == "s2" or self.name == "s3":
            self.game.all_builds.remove(self)
            self.game.spawn_build(self.pos_x, self.pos_y, f"s{str(int(self.name[1]) + 1)}", self.special)

        elif self.name == "t5" or self.name == "cave" or self.name == "fire" or self.name == "s4" or self.name == "enclos" or self.name == "trap" or self.name == "tas" or self.name == "garden":
            if self.affected_object != "axe":
                i = 1
                if self.name == "trap":
                    i = random.randint(1, 2)
                for j in range(i):
                    if self.game.random_potion_level == 0:
                        self.game.spawn_item(random.choice(self.item), self.pos_x, self.pos_y, False)
                    else:
                        self.game.spawn_item(random.choice(self.game.potion_random[self.game.random_potion_level - 1]), self.pos_x, self.pos_y, False)
                if self.name == "fire":
                    self.fuel -= 1
                    if self.fuel == 0:
                        self.max_time = None
                if self.name == "enclos":
                    self.food -= 1
                    if self.food == 0:
                        self.max_time = None
                    if len(self.item) != 1:
                        self.game.sound.play(self.item[6][4:])
            else:
                self.game.all_builds.remove(self)
                self.game.remove_item("axe", self.pos_x, self.pos_y)
                self.game.sound.play("hache")
                for i in range(3):
                    self.game.spawn_item("planks", self.pos_x, self.pos_y, False)

        elif self.name == "dead_tree":
            for i in range(3):
                self.game.spawn_item("planks", self.pos_x, self.pos_y, False)
            self.game.all_builds.remove(self)
            self.game.remove_item("axe", self.pos_x, self.pos_y)

        if self.duration != None:
            self.duration -= 1
            if self.duration <= 0:
                self.duration_end_effect()

    #détruit les batiments (EX: l'enclos une fois qu'il a été utilisé 7 fois)
    def duration_end_effect(self):
        self.game.all_builds.remove(self)
        if self.name == "t5":
            if self.special == "apple" or self.special == "magic":
                self.game.spawn_build(self.pos_x, self.pos_y, "dead_tree", "")
        if self.name == "enclos" or self.name == "trap":
            for animal in self.game.all_animals:
                if animal.pos_x == self.pos_x and animal.pos_y == self.pos_y:
                    self.game.all_animals.remove(animal)
                    break
