import pygame
import random
import math
import os
from item import Item
from build import Build
from craft import Craft
from chest_item import ChestItem
from interfaceitem import InterfaceItem
from PNJ import Pnj
from animals import Animal
from hopper import Hopper
from sound import Sound


class Game:

    def __init__(self):

        #gestion de la class item
        self.all_items = pygame.sprite.Group()

        # déplacement + position des items
        self.moving_item = []
        self.posable_item = ["t1", "chest", "trap", "big_craft_table", "fire", "cave", "s1wheat", "t1apple", "enclos", "treasure", "market", "tas", "alembic", "boat", "1", "2", "3", "4", "5", "t1magic", "kitchen", "garden", "meule", "building", "launch_pad_on", "pass"]
        self.burnable_item = {"planks": 3, "branch": 1, "leaves": 1, "coal": 5, "log": 4}
        self.item_pos = [[[], [], [], [], []],
                         [[], [], [], [], []],
                         [[], [], [], [], []],
                         [[], [], [], [], []],
                         [[], [], [], [], []]]

        self.potions = [0, 0, 0, 0, 0]
        self.build_speed_potions = 1
        self.animal_speed_potion = 1
        self.random_potion_level = 0
        self.gold_potion_time = 55
        self.potion_random = [["stone", "big_stone", "leaves", "flame", "apple", "branch", "wheat", "t1", "trap", "axe", "animals_food", "log", "planks", "coal", "flint"],
                              ["stone", "big_stone", "leaves", "flame", "apple", "branch", "wheat", "t1", "trap", "axe", "animals_food", "raw_beef", "raw_lamb", "raw_chicken", "flask", "eggs", "leather", "wool", "enormous_stone", "clay", "sand", "t1apple", "s1wheat", "silver", "coal", "enclos", "flint"],
                              ["stone", "big_stone", "leaves", "flame", "apple", "branch", "wheat", "t1", "trap", "axe", "animals_food", "raw_beef", "raw_lamb", "raw_chicken", "flask", "coal", "eggs", "leather", "wool", "enormous_stone", "clay", "coal", "sand", "t1apple", "s1wheat", "cooked_beef", "cooked_lamb", "cooked_chicken", "treasure", "chest", "gold", "diamond", "fire", "enclos", "flint", "gigantic_stone"]]

        #gestion de la class build
        self.all_builds = pygame.sprite.Group()
        self.max_time_archiver = {"t5": 280, "s4": 140, "cave": 120, "fire": 150, "enclos": 310, "tas": 300, "trap": 160, "garden": 320}
        a = random.randint(1, 5)
        self.all_builds.add(Build(self, "cave", a, random.randint(1, 5), True, ""))
        if a == 5:
            self.all_builds.add(Build(self, "t5",a - 1, 4, True, ""))
        else:
            self.all_builds.add(Build(self, "t5",a + 1, random.randint(1, 5), True, ""))

        self.crafts = []
        self.big_crafts = []
        self.kitchen_craft = []
        self.meule_craft = []
        self.actual_craft = {} #dit combien il y a de craft pour donner leur position
        self.market_trades = []
        self.all_crafts = pygame.sprite.Group()
        self.craft_offset = 0

        self.all_images = {}
        self.load_images()

        self.bg = pygame.image.load("assets/bg.png")
        self.interface = 0
        self.current = 0
        self.is_running = False
        self.writing = False
        self.delete = False
        self.delete_sell = False
        self.undeletable = ["blueprint", "building", "market", "launch_pad_on", "boat", "alembic", "pass"]
        self.item_price = {"animals_food": 2, "apple": 1, "apple_pie": 10,
                           "axe": 2, "big_stone": 2, "book": 7, "branch": 1,
                           "bread": 2, "brick": 2, "burger": 20, "candy_apple": 4,
                           "chest": 3, "clay": 1, "coal": 4, "cooked_beef": 7,
                           "cooked_lamb": 7, "cooked_chicken": 7, "diamond": 50,
                           "eggs": 3, "enclos": 13, "enormous_stone": 5, "flame": 1,
                           "flask": 5, "flint": 1, "flour": 1, "fried_eggs": 4, "frites": 2,
                           "gigantic_stone": 22, "glass": 2, "gold": 11, "hopper": 29,
                           "hot_dog": 13, "leather": 3, "leaves": 1, "log": 2, "magic": 2,
                           "magic_pot": 17, "nugget": 13, "oil": 7, "paper": 2, "pasta": 15,
                           "pickaxe": 7, "planks": 2, "plate": 14, "pot": 4, "potato": 1,
                           "raw_beef": 5, "raw_lamb": 5, "raw_chicken": 5, "sand": 1, "sawdust": 1,
                           "shard": 70, "shovel": 20, "silver": 4, "soup": 6, "star": 200, "steak": 8,
                           "stone": 1, "suger": 1, "sugar_cane": 1, "t1": 1, "tas": 24, "ticket": 70, "treasure": 20,
                           "tissu": 7, "tomato": 1, "wheat": 1, "wool": 3, "1": 25, "2": 23, "3": 50,
                           "4": 14, "5": 80, "cave": 56, "fire": 10, "garden": 26, "kitchen": 45,
                           "meule": 18, "s1wheat": 5, "t1apple": 7, "t1magic": 9, "trap": 5, "big_craft_table": 4}

        self.chest_number = {}
        self.all_interface_item = pygame.sprite.Group()
        self.all_buttons = pygame.sprite.Group()

        self.selected_builder = 1
        self.all_select_builder = ["build_helper_button", "big_craft_table", "meule", "kitchen"]

        self.font = pygame.font.Font("assets/Pacifico-Regular.ttf", 40)
        self.little_font = pygame.font.Font("assets/Pacifico-Regular.ttf", 23)
        self.min_font = pygame.font.Font("assets/Pacifico-Regular.ttf", 18)

        self.age = 1
        self.money = 0

        self.text = ""
        self.text2 = ""
        self.text3 = ""

        self.saves_names = []
        self.selected_save = 0

        self.all_pnj = pygame.sprite.Group()
        self.pnj_time = 0
        self.pnj_names = ["archere", "barbarian", "dragon", "hunter", "randonneur"]

        self.all_animals = pygame.sprite.Group()
        self.animal_time = 0

        self.is_tips = False
        self.tips_image = pygame.image.load("assets/tips_interface.png")
        self.tips_co_x = 1580
        self.tips_time = 0
        self.all_tips = {"boat": False, "enclos": False, "trap": False, "axe": False, "pickaxe": False, "market": False, "animal_food_enclos": False, "big_craft_table": False, "alembic": False, "blueprint": False, "s1wheat": False, "building": False, "t1apple": False, "kitchen": False, "meule": False}

        self.boat_trades = []
        self.boat_image = pygame.transform.scale(pygame.image.load("assets/builds/boat.png"), (130, 130))
        self.boat_rect = self.boat_image.get_rect()
        self.boat_rect.x = 1100
        self.boat_rect.y = 560
        self.boat_time = 0
        self.boat_wave = pygame.image.load("assets/boat_wave.png")

        self.pass_trades = []

        self.set_crafts()
        i = True
        while i:
            self.boat_trade = random.choice(self.boat_trades)
            if self.boat_trade[0] == 2:
                i = False

        self.all_images_to_draw = []

        self.is_building = False
        self.building = []

        self.actual_market_trades = []
        self.change_market_trade_time = 0

        self.fusee_image = pygame.transform.scale(pygame.image.load("assets/builds/fusee.png"), (144, 144))
        self.fusee_rect_x = 0
        self.fusee_rect_y = 0
        self.fusee_pad_rect_y = 0
        self.fusee_time = 0
        self.fusee_fuel = 0

        self.all_hopper = pygame.sprite.Group()

        self.pass_number = 1

        self.sound = Sound()

    #permet de vérifier si une liste est présente à l'intérieur d'une autre
    def list_in_list(self, list1, list2):
        i = []
        t = 0
        for element in list1:
            if element in list2:
                t += 1
                i.append(element)
                list2.remove(element)
        for j in i:
            list2.append(j)
        if t == len(list1):
            return True

    #permet de convertir les position des objets (la case) en position par pixel en x et y
    def co_to_pos(self, co_x, co_y):
        if 320 <= co_x <= 448:
            pos_x = 1
        elif 448 < co_x <= 576:
            pos_x = 2
        elif 576 < co_x <= 704:
            pos_x = 3
        elif 704 < co_x <= 832:
            pos_x = 4
        elif 832 < co_x <= 960:
            pos_x = 5

        if 45 <= co_y <= 173:
            pos_y = 1
        elif 173 < co_y <= 301:
            pos_y = 2
        elif 301 < co_y <= 429:
            pos_y = 3
        elif 429 < co_y <= 557:
            pos_y = 4
        elif 557 < co_y <= 690:
            pos_y = 5

        return (pos_x, pos_y)

    #charge toutes les images au lancement du jeu
    def load_images(self):
        for image in os.listdir("assets/items"):
            loaded_image = pygame.image.load(f"assets/items/{image}")
            self.all_images[image[0:len(image) - 4]] = pygame.transform.scale(loaded_image, (40, 40))

        for image in os.listdir("assets/builds"):
            if image[0:len(image) - 4] in self.posable_item:
                loaded_image = pygame.image.load(f"assets/builds/{image}")
                self.all_images[image[0:len(image) - 4]] = pygame.transform.scale(loaded_image, (40, 40))

    #charge tous les crafts au chargement du jeu
    def set_crafts(self):
        with open("crafts/classic.txt", "r+") as craft:
            recipes = []
            for line in craft:
                value = line.split()
                recipes.append(value)

        craft.close()

        for i in range(len(recipes)):
            self.crafts.append({"recipe": recipes[i][2:], "product": recipes[i][1], "age": int(recipes[i][0])})

        with open("crafts/big_crafts.txt", "r+") as craft:
            recipes = []
            for line in craft:
                value = line.split()
                recipes.append(value)

        craft.close()

        for i in range(len(recipes)):
            self.big_crafts.append({"recipe": recipes[i][2:], "product": recipes[i][1], "age": int(recipes[i][0])})

        with open("crafts/market.txt", "r+") as market:
            recipes = []
            for line in market:
                value = line.split()
                recipes.append(value)

        craft.close()

        for i in range(len(recipes)):
            self.market_trades.append({"item": recipes[i][2], "price": int(recipes[i][1]), "age": int(recipes[i][0])})

        with open("crafts/boat.txt", "r+") as boat:
            for line in boat:
                line = line.split()
                self.boat_trades.append([int(line[0]), int(line[1]), int(line[2]), int(line[3]), int(line[4]), int(line[5]), int(line[6]), line[7]])
            boat.close()

        with open("crafts/kitchen.txt", "r+") as kitchen:
            recipes = []
            for line in kitchen:
                value = line.split()
                recipes.append(value)

        craft.close()

        for i in range(len(recipes)):
            self.kitchen_craft.append({"product": recipes[i][1], "recipe": recipes[i][2:], "age": int(recipes[i][0])})

        with open("crafts/meule.txt", "r+") as meule:
            recipes = []
            for line in meule:
                value = line.split()
                recipes.append(value)

        craft.close()

        for i in range(len(recipes)):
            self.meule_craft.append({"recipe": recipes[i][1], "product": recipes[i][2:], "age": int(recipes[i][0])})

        prices = []
        products = []
        with open("crafts/pass/price.txt", "r+") as price:
            for line in price:
                line = line.split()
                prices.append(line)
            price.close()
        with open("crafts/pass/product.txt", "r+") as product:
            for line in product:
                line = line.split()
                products.append(line)
            product.close()
        for i in range(25):
            self.pass_trades.append([prices[i], products[i]])

    #regroupe toutes les actions à éxécuter quand le joueur appuie sur une croix
    def cross(self):
        if self.current == 2 or self.current == 9:
            self.sound.play("chest")
        self.interface = 0
        self.selected_builder = 1
        self.all_interface_item = pygame.sprite.Group()
        self.all_images_to_draw = []
        for button in self.all_buttons:
            if button.name == "market_trade" or button.name == "cross" or button.name == "arrow_up" or button.name == "arrow_down":
                self.all_buttons.remove(button)
        if self.is_running:
            self.current = 1
            self.craft_offset = 0
            self.see_craft()
            i = ""
            if self.delete == False:
                i = "_off"
            self.spawn_button(f"delete{i}", 100, 10, 50)
            self.spawn_button("build_helper_button", 30, 10, 50)
            if self.age >= 2:
                self.spawn_remove()
                if self.text3 == "Commerce et Agriculture":
                    self.reset_tips("Utilisez des combustibles", "pour allumer le feu et", "obtenir des flammes")
                if self.text3 == "Industrialisation":
                    self.reset_tips("Le centre commercial", " rassemble plusieurs", "batiments en un seul")
        else:
            self.current = 0

    #permet de faire apparaitre la croix quand le joueur ouvre une interface
    def spawn_cross(self):
        self.all_buttons.add(InterfaceItem(self, 1156, 20, "cross", 75))
        self.update_cross_pos()

    # détecte les crafts présents sur le terrain
    def see_craft(self):
        if self.interface != 2:
            self.actual_craft = {}
            self.all_crafts = pygame.sprite.Group()
            self.craft_offset = 0

            for pnj in self.all_pnj:
                self.all_crafts.add(Craft(self, pnj.recipe, pnj.product, "", 980, pnj.rect.y + 110))

            for line in range(5):
                for colonne in range(5):
                    for craft in self.crafts:
                        if self.age >= craft["age"]:
                            if self.list_in_list(craft["recipe"], self.item_pos[line][colonne]) == True and craft["product"] not in self.actual_craft:
                                self.actual_craft[craft["product"]] = craft["recipe"]
                                self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "", 15, 0))

            if self.current == 11:
                self.open_boat()

            for button in self.all_buttons:
                if button.name == "arrow_up" or button.name == "arrow_down":
                    self.all_buttons.remove(button)
                    break

            if len(self.actual_craft) > 7:
                self.spawn_button("arrow_down", 140, 680, 30)
            self.arrow_button(0)

    #permet de décaler les items quand ils sont plusieurs sur la même case
    def shift_item(self, pos_x, pos_y):
        items = []

        if self.age >= 4: #faire bouger les items quans il y a un hopper
            l_m_i = self.moving_item
            for hopper in self.all_hopper:
                for item in self.all_items:
                    if item.pos_x == hopper.pos_x and item.pos_y == hopper.pos_y and item.affected == False:
                        if 0 < hopper.pos_x + hopper.direction[0] < 6 and 0 < hopper.pos_y + hopper.direction[1] < 6:
                            self.moving_item = [item.pos_x, item.pos_y]
                            self.move_item(item, (hopper.pos_x + hopper.direction[0], hopper.pos_y + hopper.direction[1]), False)
            self.moving_item = l_m_i

        for item in self.all_items: # pour décaler les items
            if item.pos_x == pos_x and item.pos_y == pos_y and item.affected == False:
                item.rect.x = 273 + 128 * item.pos_x
                items.append(item)
        for i in range(len(items)):
            items[i].rect.x -= i*40

        for index, build in enumerate(self.all_builds):  # pour que le build s'arrete de fonctionner quand y'a 3 items dessus
            if build.pos_x == pos_x and build.pos_y == pos_y:
                if build.name in self.max_time_archiver.keys():
                    if len(items) == 3:
                        build.max_time = None
                    else:
                        build.max_time = self.max_time_archiver[build.name]
                        if build.name == "fire":
                            if build.fuel == 0:
                                build.max_time = None
                        elif build.name == "enclos":
                            if build.food == 0:
                                build.max_time = None
                        elif build.name == "t5":
                            if build.affected_object == "axe":
                                build.max_time = 150
                                if len(items) == 3:
                                    build.max_time = None
                        elif build.name == "trap":
                            r = False
                            for animal in self.all_animals:
                                if animal.pos_x == build.pos_x and animal.pos_y == build.pos_y:
                                    r = True
                            if r == False:
                                build.max_time = None

                break

    #change les position de la croix (en fonction de si c'est une petite ou grande interface)
    def update_cross_pos(self):
        for button in self.all_buttons:
            if button.name == "cross":
                if self.interface == 2:
                    button.rect.x = 1156
                    button.rect.y = 20
                elif self.interface == 1:
                    button.rect.x = 886
                    button.rect.y = 167

    #fait apparaitre un item sur le terrain à une certaine coordonnée
    def spawn_item(self, name, pos_x, pos_y, affected):
        i = 0
        for item in self.all_items:
            if item.pos_x == pos_x and item.pos_y == pos_y and item.affected == False:
                i += 1
        if i < 3:
            self.all_items.add(Item(self, name, pos_x, pos_y, affected))
            self.item_pos[pos_y - 1][pos_x - 1].append(name)
            self.shift_item(pos_x, pos_y)
            self.see_craft()
            if self.current == 3:
                self.see_big_crafts()
            elif self.current == 10:
                self.see_kitchen_craft()
            elif self.current == 14:
                self.see_meule_craft()
            return True

    # fait apparaitre un item sur le terrain n'import où
    def force_spawn_item(self, name):
        s = False
        for ligne in range(5):
            if s == False:
                for colonne in range(5):
                    if len(self.item_pos[ligne][colonne]) < 3:
                        self.all_items.add(Item(self, name, colonne + 1, ligne + 1, False))
                        self.item_pos[ligne][colonne].append(name)
                        self.shift_item(colonne + 1, ligne + 1)
                        s = True
                        break

        self.see_craft()

    #fait apparaitre un batiment
    def spawn_build(self, pos_x, pos_y, name, special):
        self.all_builds.add(Build(self, name, pos_x, pos_y, True, special))

    #fait apparaitre les sauvegardes quand le joueur clique sur "sauvegarde"
    def spawn_save(self):
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.saves_names = os.listdir("saves")
        for index, save in enumerate(self.saves_names):
            self.all_buttons.add(InterfaceItem(self, 370, index * 100 + 220, "save" + str(index + 1), 80))

    #fait apparaitre les 3 boutons quand on ouvre une sauvegarde
    def spawn_buttons_save(self):
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.text = "rename"
        i = ["play", "rename", "delete"]
        for j in range(len(i)):
            self.all_buttons.add(InterfaceItem(self, 370, j * 100 + 220, i[j], 80))

    #fait apparaitre un bouton
    def spawn_button(self, name, co_x, co_y, scale):
        i = True
        if name == "arrow_up" or name == "arrow_down":
            for button in self.all_buttons:
                if button.name == name:
                    i = False
                    break
        if i:
            self.all_buttons.add(InterfaceItem(self, co_x, co_y, name, scale))

    #enlève un item du jeu à partir de position
    def remove_item(self, name, pos_x, pos_y):
        for item in self.all_items:
            if item.pos_x == pos_x and item.pos_y == pos_y and item.name == name:
                self.item_pos[pos_y - 1][pos_x - 1].remove(name)
                self.all_items.remove(item)

    #enlèvre l'item du jeu à partir de l'item lui même
    def remove_item_fast(self, item):
        self.item_pos[item.pos_y - 1][item.pos_x - 1].remove(item.name)
        self.all_items.remove(item)
        self.see_craft()
        self.shift_item(item.pos_x, item.pos_y)

    #déplace un item et éxécute toutes les fonctions associées (EX: burn_item() quand on bouge un combustible)
    def move_item(self, item, co, is_co): # co = coordonnées de destination / is_co pour savoir si les coordonées sont données en "co" ou en "pos"
        i = 0
        j = False
        if is_co:
            x, y = self.co_to_pos(co[0], co[1])
        else:
            x, y = co[0], co[1]
        for itemx in self.all_items:
            if itemx.pos_x == x and itemx.pos_y == y and not itemx.affected:
                i += 1
        if i < 3:
            item.can_move = False
            item.rect.x = 273 + 128 * x
            item.rect.y = 5 + 128 * y

            last_pos_x, last_pos_y = item.pos_x, item.pos_y

            item.pos_x, item.pos_y = x, y

            self.item_pos[self.moving_item[1] - 1][self.moving_item[0] - 1].remove(item.name)
            self.item_pos[item.pos_y - 1][item.pos_x - 1].append(item.name)

            j = True

        if item in self.all_items:
            if item.name in self.burnable_item.keys() or item.name == "flame":
                self.burn_item(item, x, y)

            elif item.name == "animals_food":
                self.feed_animals(item, x, y)

            elif item.name == "oil":
                self.fusee_oil(item, x, y)

            elif item.name == "star":
                self.affect_star(item, x, y)

            if item in self.all_items: # permet de savoir si l'item à été utilisé (EX: cuisine, combustible...)
                self.put_in_chest(item, x, y)

            if item in self.all_items and j:
                self.shift_item(item.pos_x, item.pos_y)
                self.shift_item(last_pos_x, last_pos_y)

            elif j:
                self.shift_item(last_pos_x, last_pos_y)

        i = True
        if item in self.all_items:
            i = False
        if j:
            i = True
        if i:
            self.see_craft()
        return i

    #construit un batiment et éxécute le code associé (EX: rassembler les batiments dans le building)
    def build_item(self, item, pos):
        pos_x, pos_y = self.co_to_pos(pos[0], pos[1])
        for build in self.all_builds:
            if build.pos_x == pos_x and build.pos_y == pos_y:
                return False
        for animal in self.all_animals:
            if animal.pos_x == pos_x and animal.pos_y == pos_y:
                return False

        if item.name == "ticket":
            if "pass" in self.building:
                if self.pass_number % 5 != 0:
                    for index, trade in enumerate(self.pass_trades):
                        if index == self.pass_number:
                            for itemx in trade[1]:
                                self.force_spawn_item(itemx)
                            self.pass_number += 1
                            self.remove_item_fast(item)
                            self.moving_item = []
                            return True
                    return False
                elif self.pass_number % 5 == 0:
                    self.reset_tips("Vous ne pouvez pas", "passer la", "prochine étape")
                    return False
            else:
                self.reset_tips("Vous devez d'abord", "obtenir la roue", "de la chance")
                return False

        self.remove_item_fast(item)
        self.moving_item = []

        if item.name == "enclos" and self.all_tips["enclos"] == False:
            self.all_tips["enclos"] = True
            self.reset_tips("Attendez que les animaux", "se piègent dans l'enclos pour", "récupérer des ressources")
        elif item.name == "trap" and self.all_tips["trap"] == False:
            self.all_tips["trap"] = True
            self.reset_tips("Attendez que les animaux", "aillent sur le piège pour", "récupérer leur viande")
        elif item.name == "market" and self.all_tips["market"] == False:
            self.all_tips["market"] = True
            self.reset_tips("Achetez des ressources", "au marché. Les achats", "changent régulièrement")
        elif item.name == "big_craft_table" and self.all_tips["big_craft_table"] == False:
            self.all_tips["big_craft_table"] = True
            self.reset_tips("Il n'y a pas besoin de", "mettre les objets sur l'établi", "pour fabriquer des objets")
        elif item.name == "kitchen" and self.all_tips["kitchen"] == False:
            self.reset_tips("La cuisine permet de faire", "de la nourriture souvent", "nécessaire pour le commerce")
        elif item.name == "meule" and self.all_tips["meule"] == False:
            self.reset_tips("Le mortier permet d'écraser", "les objets", "")

        elif item.name == "fire" and self.age == 1:
            self.new_age()

        elif item.name == "alembic":
            self.new_age()

        elif item.name == "building":
            self.new_age()

        elif item.name == "launch_pad_on":
            self.fusee_rect_x = pos_x * 128 + 173
            self.fusee_rect_y = pos_y * 128 - 73
            self.fusee_pad_rect_y = self.fusee_rect_y

        if item.name[0:2] == "t1" or item.name[0:2] == "s1":
            self.all_builds.add(Build(self, item.name[0:2], pos_x, pos_y, True, item.name[2:]))

        elif item.name[0] in ["1", "2", "3", "4", "5"]:

            if item.name[0] == "1":
                if item.name[2] in ["1", "2"]:
                    self.build_speed_potions = int(item.name[2]) * 2
                else:
                    self.build_speed_potions = 8
                self.potions[0] = 1800

            elif item.name[0] == "2":
                if item.name[2] in ["1", "2"]:
                    self.animal_speed_potion = int(item.name[2]) * 3
                else:
                    self.animal_speed_potion = 11
                self.potions[1] = 2000

            elif item.name[0] == "3":
                if item.name[2] == "1":
                    self.potions[2] = 3600
                elif item.name[2] == "2":
                    self.potions[2] = 5000
                else:
                    self.potions[2] = 8500
                self.gold_potion_time = 55

            elif item.name[0] == "4":
                self.potions[3] = 1200
                for build in self.all_builds:
                    self.rotate(build)
                for item in self.all_items:
                    self.rotate(item)
                for animal in self.all_animals:
                    self.rotate(animal)
                for pnj in self.all_pnj:
                    self.rotate(pnj)
                for button in self.all_buttons:
                    self.rotate(button)
                all_images = {}
                for name, image in self.all_images.items():
                    all_images[name] = pygame.transform.flip(image, False, True)
                self.all_images = all_images
                self.boat_image = pygame.transform.flip(self.boat_image, False, True)

            elif item.name[0] == "5":
                self.potions[4] = 1600
                self.random_potion_level = int(item.name[2])

        elif item.name == "building":
            self.is_building = True
            i = ["alembic", "big_craft_table", "meule", "kitchen", "market"]
            self.all_builds.add(Build(self, item.name, pos_x, pos_y, True, ""))
            for build in self.all_builds:
                if build.name in i:
                    if build.name == "kitchen":
                        for buildx in self.all_builds:
                            if buildx.name == "building":
                                buildx.fuel = build.fuel
                    self.all_builds.remove(build)
                    self.building.append(build.name)
                    i.remove(build.name)
            self.all_tips["building"] = True

        else:
            self.all_builds.add(Build(self, item.name, pos_x, pos_y, True, ""))

        if item.name in ["alembic", "big_craft_table", "meule", "kitchen", "market", "pass"] and self.is_building and item.name not in self.building:
            self.reset_tips("le batiment à été ajouté", "au centre commercial", "")
            self.building.append(item.name)
            for build in self.all_builds:
                if build.pos_x == pos_x and build.pos_y == pos_y:
                    self.all_builds.remove(build)
                    break

    #permet d'afecter un item (pioche, hache...)
    def affect_item(self, item, pos):
        r = False
        if item.name in ["axe", "pickaxe", "shovel"]:
            for build in self.all_builds:
                if build.pos_x == self.co_to_pos(pos[0], pos[1])[0] and build.pos_y == self.co_to_pos(pos[0], pos[1])[1] and build.affected_object == None and item.name == build.can_affect:
                    self.shift_item(item.pos_x, item.pos_y)

                    # ajout spéciaux en fontion des items
                    if item.name == "pickaxe":
                        item.add_pickaxe(build, True)
                    elif item.name == "axe":
                        item.add_axe(build, True)
                    elif item.name == "shovel":
                        item.add_shovel(build, True)
                    build.affected_object = item.name

                    # on change les cos de l'item dans self.item_pos
                    lpx = item.pos_x
                    lpy = item.pos_y
                    self.item_pos[lpy - 1][lpx - 1].remove(item.name)
                    item.pos_x = build.pos_x
                    item.pos_y = build.pos_y
                    self.item_pos[item.pos_y - 1][item.pos_x - 1].append(item.name)

                    # on change les cos de l'item dans l'item
                    item.rect.x = build.rect.x
                    item.rect.y = build.rect.y - 15
                    if item.affected == True:
                        self.unaffect_item(item, lpx, lpy, item.rect.x, item.rect.y, True)
                    item.affected = True
                    item.can_move = False
                    r = True
                    break
            if r:
                self.see_craft()
                self.shift_item(lpx, lpy)
        return r

    #enlève l'affectation d'un item
    def unaffect_item(self, item, pos_x, pos_y, new_co_x, new_co_y, come_from_affectation):
        r = False
        for build in self.all_builds:
            if build.pos_x == pos_x and build.pos_y == pos_y:     #si le build correspond aux anciennes coordonées de item
                if build.affected_object != None:                       #si il a bien un objet à désafecter
                    if come_from_affectation == True:       #permet de savoir si la fonction est appelée suite à une fonction (et donc que l'item à déja été bougé)
                        r = True
                    else:
                        if self.move_item(item, (new_co_x, new_co_y), True) == True:
                            r = True
                    if r:
                        if build.affected_object == "pickaxe":
                            item.add_pickaxe(build, False)
                        elif build.affected_object == "axe":
                            item.add_axe(build, False)
                        elif build.affected_object == "shovel":
                            item.add_shovel(build, False)
                        build.affected_object = None
                        item.affected = False
                        if come_from_affectation:  # si l'item viens d'une affectation il faut le remettre en "affecté"
                            item.affected = True
                        self.shift_item(item.pos_x, item.pos_y)
                        self.see_craft()
                        return True
                else:
                    break

    #permet de fabriquer un item avec les crafts présents sur le jeu principal (les crafts normaux)
    def craft_item(self, craft):
        r = True
        for line in range(5):
            for colonne in range(5):
                if self.list_in_list(craft.recipe, self.item_pos[line][colonne]) == True and r:
                    i = 0
                    for item in self.all_items:
                        if item.pos_y == line + 1 and item.pos_x == colonne + 1 and item.name in craft.recipe:
                            if item.can_move == True:
                                self.moving_item = []
                            self.all_items.remove(item)
                            self.item_pos[line][colonne].remove(item.name)
                            i += 1
                            if i == len(craft.recipe):
                                r = False
                                break
                    self.all_items.add(Item(self, craft.product, colonne + 1, line + 1, False))
                    self.sound.play("clou")
                    self.item_pos[line][colonne].append(craft.product)
                    self.shift_item(colonne + 1, line + 1)
                    self.see_craft()
                    if craft.product in self.all_tips.keys():
                        if self.all_tips[craft.product] == False:
                            self.all_tips[craft.product] = True
                            if craft.product == "axe":
                                self.reset_tips("utilisez la hache", "sur un arbre", "pour couper du bois")
                            elif craft.product == "pickaxe":
                                self.reset_tips("utilisez la pioche", "sur une grotte", " pour récupérer des minéraux")

                    break

    #ouverture de l'interface de l'établit
    def open_big_craft_table(self):
        self.current = 3
        self.interface = 2
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.see_big_crafts()

    #détecte tous les big_crafts possible
    def see_big_crafts(self):
        self.actual_craft = {}
        self.all_crafts = pygame.sprite.Group()
        self.all_images_to_draw = []
        self.all_interface_item = pygame.sprite.Group()
        items = []
        for item in self.all_items:
            if item.affected == False:
                items.append(item.name)

        for craft in self.big_crafts:
            if self.age >= craft["age"]:
                if self.list_in_list(craft["recipe"], items):
                    self.actual_craft[craft["product"]] = craft["recipe"]
                    self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 85 - 10))

        if len(self.actual_craft) > 7 and self.craft_offset + 7 < len(self.actual_craft):
            self.spawn_button("arrow_down", 1220, 385, 50)
            self.arrow_button(11)
        if self.craft_offset > 0:
            self.spawn_button("arrow_up", 1220, 305, 50)
            self.arrow_button(11)
        else:
            self.set_image_to_craft()

    #ouverture de l'interface du coffre
    def open_chest(self, build):
        self.all_interface_item = pygame.sprite.Group()
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.interface = 2
        self.update_cross_pos()
        if build.name == "chest":
            self.current = 2
            self.chest_number = build.chest_item
            for index, item in enumerate(build.chest_item.items()):
                i = 250
                if index % 2 == 1:
                    i += 250
                self.all_interface_item.add(ChestItem(build, math.ceil((index + 1) / 2) * 207 - 11, i - 100, item[0], False))

        if build.name == "treasure":
            self.current = 9
            for index, item in enumerate(build.items_in):
                self.all_interface_item.add(ChestItem(build, math.ceil((index + 1) / 5) * 120 - 5, index % 5 * 120 + 70, item, False))

    #affiche les nombres dans le coffre
    def blit_chest_number(self, screen):
        for index, number in enumerate(self.chest_number.items()):
            i = 250
            if index % 2 == 1:
                i += 250
            screen.blit(self.font.render(str(number[1]), 1, (100, 20, 20)), (math.ceil((index + 1) / 2) * 207 + 19, i))

    # détecte quand le joueur met un item dans le coffre
    def put_in_chest(self, item, pos_x, pos_y):
        if item.name[0] not in ["1", "2", "3", "4", "5"]:
            if item in self.all_items:
                is_already_in = False
                for build in self.all_builds:
                    if build.pos_x == pos_x and build.pos_y == pos_y:
                        if build.name == "chest":
                            for key in build.chest_item.keys():
                                if key == item.name:
                                    build.chest_item[key] += 1
                                    self.remove_item_fast(item)
                                    is_already_in = True
                                    break
                            if is_already_in == False:
                                if len(build.chest_item) < 10:
                                    build.chest_item[item.name] = 1
                                    if self.current == 2:
                                        i = 250
                                        if (len(build.chest_item) - 1) % 2 == 1:
                                            i += 250
                                        self.all_interface_item.add(ChestItem(build, math.ceil(len(build.chest_item) / 2) * 207 - 11, i - 100, item.name, False))
                                    self.remove_item_fast(item)
                                    break
                        elif build.name == "treasure":
                            if len(build.items_in) < 45 and build.name == "treasure":
                                build.items_in.append(item.name)
                                self.remove_item_fast(item)

                        else:
                            break
                        break

    #retire un item du coffre
    def remove_form_chest(self, item):
        if len(self.item_pos[item.build.pos_y - 1][item.build.pos_x - 1]) < 3:
            self.spawn_item(item.name, item.build.pos_x, item.build.pos_y, False)
            item.build.chest_item[item.name] -= 1
            if item.build.chest_item[item.name] <= 0:
                del item.build.chest_item[item.name]
                self.open_chest(item.build)

    #retire un item du treasure (treasure = le coffre spécial)
    def remove_from_treasure(self, item):
        if len(self.item_pos[item.build.pos_y - 1][item.build.pos_x - 1]) < 3:
            self.spawn_item(item.name, item.build.pos_x, item.build.pos_y, False)
            item.build.items_in.remove(item.name)
            self.open_chest(item.build)

    #brule un item dans le feu si c'est un combutible
    def burn_item(self, item, pos_x, pos_y):
        for buildx in self.all_builds:
            if buildx.pos_x == pos_x and buildx.pos_y == pos_y:
                if buildx.name == "fire" and item.name in self.burnable_item.keys() or buildx.name == "kitchen" and item.name == "flame" or buildx.name == "building" and item.name == "flame":
                    if buildx.name == "fire":
                        buildx.fuel += self.burnable_item[item.name]
                        buildx.max_time = 150
                        self.shift_item(buildx.pos_x, buildx.pos_y)
                    else:
                        buildx.fuel += 1
                    self.remove_item_fast(item)
                    break
                else:
                    break

    #interface de l'aide aux crafts
    def open_build_helper(self):
        self.current = 4
        self.interface = 2
        self.update_cross_pos()
        self.all_crafts = pygame.sprite.Group()
        self.all_buttons = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.spawn_cross()
        self.actual_craft = {}
        self.craft_offset = 0
        for index, button in enumerate(self.all_select_builder):
            i = False
            if button == "build_helper_button":
                i = True
            elif button in self.building:
                i = True
            else:
                for build in self.all_builds:
                    if build.name == button:
                        i = True
                        break
            if i:
                self.all_interface_item.add(InterfaceItem(self, index * 80 + 200, 80, button, 50))
        self.see_build_helper_crafts()

    #détecte tous les crafts à mettre dans l'aide au crafts
    def see_build_helper_crafts(self):
        items = []
        for item in self.all_items:
            items.append(item.name)

        if self.selected_builder == 1:
            i = 0
            for craft in self.crafts:
                if craft["age"] <= self.age:
                    for item in craft["recipe"]:
                        if item in items:
                            self.actual_craft[craft["product"]] = craft["recipe"]
                            self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "", 350 * (i % 3) + 150, math.ceil((i + 1) / 3) * 85 + 65))
                            i += 1
                            break

        elif self.selected_builder == 2:
            for craft in self.big_crafts:
                if craft["age"] <= self.age:
                    for item in craft["recipe"]:
                        if item in items:
                            self.actual_craft[craft["product"]] = craft["recipe"]
                            self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 87 + 55))
                            break

        elif self.selected_builder == 4:
            for craft in self.kitchen_craft:
                if craft["age"] <= self.age:
                    for item in craft["recipe"]:
                        if item in items:
                            self.actual_craft[craft["product"]] = craft["recipe"]
                            self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 87 + 55))
                            break

        elif self.selected_builder == 3:
            for craft in self.meule_craft:
                if craft["age"] <= self.age:
                    if craft["recipe"] in items:
                        self.actual_craft[craft["recipe"]] = craft["product"]
                        self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "meule", (len(self.actual_craft) + 1) % 2 * 500 + 200, math.ceil(len(self.actual_craft) / 2) * 87 + 55))

        if self.selected_builder == 1 and len(self.actual_craft) > 18 and self.craft_offset + 18 < len(self.actual_craft) \
                or self.selected_builder in [2, 4] and len(self.actual_craft) > 6 and self.craft_offset + 6 < len(self.actual_craft):
            self.spawn_button("arrow_down", 1220, 385, 50)
            self.arrow_button(self.selected_builder)
        if self.craft_offset > 0:
            self.spawn_button("arrow_up", 1220, 305, 50)
            self.arrow_button(self.selected_builder)
        self.set_image_to_craft()

    #associe toutes les images nécessaire à un crafts
    def set_image_to_craft(self):
        self.all_images_to_draw = []
        i = False
        if self.selected_builder == 3 and self.current == 4:
            i = True
        if self.age > 3:
            if self.building[self.selected_builder - 1] == "meule" and self.current == 15:
                i = True
        if i:
            for index, craft in enumerate(self.all_crafts):
                self.all_images_to_draw.append(self.all_images[craft.recipe])
                self.all_images_to_draw.append((craft.rect.x + 20, craft.rect.y + 15))
                for index, item in enumerate(craft.product):
                    self.all_images_to_draw.append(self.all_images[item])
                    self.all_images_to_draw.append((craft.rect.x + index * 55 + 130, craft.rect.y + 15))
        else: # uniquement pour la meule
            for craft in self.all_crafts:
                i = 220
                if self.selected_builder == 2 or self.selected_builder == 4 or self.current == 10 or self.current == 3 or self.current == 15:
                    i = 920
                self.all_images_to_draw.append(self.all_images[craft.product])
                self.all_images_to_draw.append((craft.rect.x + i, craft.rect.y + 15))
                for index, item in enumerate(craft.recipe):
                    self.all_images_to_draw.append(self.all_images[item])
                    self.all_images_to_draw.append((craft.rect.x + index * 55 + 25, craft.rect.y + 15))

    #fait apparaitre un PNJ
    def spawn_pnj(self):
        banned_names = []
        for pnj in self.all_pnj:
            banned_names.append(pnj.name)
            self.pnj_names.remove(pnj.name)

        self.pnj_time = 0
        self.all_pnj.add(Pnj(self, random.choice(self.pnj_names)))

        for name in banned_names:
            self.pnj_names.append(name)
        for button in self.all_buttons:
            if button.name == "remove":
                self.all_buttons.remove(button)
        self.updade_pnj_pos()
        if self.interface != 2:
            self.spawn_remove()
        self.see_craft()

    # décale les PNJ (EX: quand il y en a 2)
    def updade_pnj_pos(self):
        i = 125
        for pnj in self.all_pnj:
            pnj.rect.y = i
            i += 180

        self.see_craft()

    #permet de faire les gris crafts (EX: cuisine, big_craft_table...)
    def crafting(self, craft):
        items = []
        for item in self.all_items:
            if item.affected == False:
                items.append(item.name)

        if isinstance(craft.recipe, str):
            craft.recipe = [craft.recipe]

        if self.list_in_list(craft.recipe, items):
            for itemx in craft.recipe:
                for itemy in self.all_items:
                    if itemx == itemy.name:
                        self.remove_item_fast(itemy)
                        break

            if self.current == 3:
                self.sound.play("clou")
            elif self.current == 13:
                self.sound.play("potion_craft")

            if self.age >= 4 and self.current == 15:
                if self.building[self.selected_builder - 1] == "big_craft_table":
                    self.sound.play("clou")
                elif self.building[self.selected_builder - 1] == "potion_craft":
                    self.sound.play("potion_craft")

            if isinstance(craft.product, (int)) == False:
                if craft.product not in ["1", "2", "3", "4", "5"]:
                    if isinstance(craft.product, list):
                        for item in craft.product:
                            self.force_spawn_item(item)
                    else:
                        self.force_spawn_item(craft.product)
                else:
                    if craft.recipe[-1] == "silver":
                        craft.product += "-1"
                    elif craft.recipe[-1] == "gold":
                        craft.product += "-2"
                    else:
                        craft.product += "-3"
                    self.force_spawn_item(craft.product)
            else:
                self.money += craft.product

            if craft.product == "s1wheat" and self.interface == 3:
                self.reset_tips("la fabrication de la graine", "de blé est à usage unique.", "Elle permet d'obtenir du blé")
                self.big_crafts.remove({'recipe': ['t1', 't1', 't1', 't1', 't1'], 'product': 's1wheat', 'age': 2})
            elif craft.product == "t1apple" and self.interface == 3:
                self.reset_tips("la fabrication du pommier", "est à usage unique. Elle", "permet d'obtenir des pommes")
                self.big_crafts.remove({'recipe': ["s1wheat", "t1", "t1", "t1", "flame", "flame", "flame"], 'product': 't1apple', 'age': 2})
            elif craft.product == "market":
                save_trade = []
                for i in range(6):
                    j = True
                    while j:
                        t = random.choice(self.market_trades)
                        if t["age"] <= self.age:
                            self.market_trades.remove(t)
                            save_trade.append(t)
                            self.actual_market_trades.append(t["item"])
                            j = False
                for trade in save_trade:
                    self.market_trades.append(trade)
                self.big_crafts.remove({'recipe': ['planks', 'planks', 'planks', 'planks', 'apple', 'apple', 'gigantic_stone', 'gold'], 'product': 'market', 'age': 2})

            self.cross()

            if self.current == 1:
                for pnj in self.all_pnj:
                    if pnj.recipe == craft.recipe and pnj.product == craft.product:
                        self.all_pnj.remove(pnj)
                        break
                for button in self.all_buttons:
                    if button.name == "remove":
                        self.all_buttons.remove(button)
                self.updade_pnj_pos()
                self.spawn_remove()

    #fait apparaitre un animal
    def spawn_animal(self):
        self.animal_time = 0
        for i in range(5):
            if random.randint(0, 1) == 0:
                l = random.choice([1, 5])
                c = random.randint(1, 5)
            else:
                c = random.choice([1, 5])
                l = random.randint(1, 5)
            r = True
            a = ["beef", "lamb", "chicken"]
            if self.age >= 3:
                a.append("fairy")
            animal = Animal(self, random.choice(a), l, c)

            for build in self.all_builds:
                if build.pos_x == l and build.pos_y == c:
                    r = False

                    if build.name == "enclos" or build.name == "trap":
                        r = True
                    break
            for animalx in self.all_animals:
                if animalx.pos_x == l and animalx.pos_y == c:
                    r = False
                    break

            if r:
                self.all_animals.add(animal)
                for build in self.all_builds:
                    if build.pos_x == animal.pos_x and build.pos_y == animal.pos_y:
                        if build.name == "enclos":
                            animal.lock = True
                            if animal.name == "beef":
                                build.item = ["leather", "leather", "leather", "leather", "leather", "leather", "raw_beef"]
                            elif animal.name == "lamb":
                                build.item = ["wool", "wool", "wool", "wool", "wool", "wool", "raw_lamb"]
                            elif animal.name == "chicken":
                                build.item = ["eggs", "eggs", "eggs", "eggs", "eggs", "eggs", "raw_chicken"]
                            elif animal.name == "fairy":
                                build.item = ["magic"]
                            if self.all_tips["animal_food_enclos"] == False:
                                self.all_tips["animal_food_enclos"] = True
                                self.reset_tips("utilisez de la nourriture", "pour animaux afin", "de récupérer des ressources")

                        elif build.name == "trap":
                            animal.lock = True
                            build.max_time = 160
                            if animal.name != "fairy":
                                build.item = ["raw_" + animal.name]
                            else:
                                build.item = ["magic"]
                            build.duration = 1
                break

    #fait se déplacer un animal
    def move_animal(self, animal):
        animal.time = 0
        m = True
        l = 0
        c = 0
        for i in range(5):
            if random.randint(0, 1) == 0:
                l = random.choice([-1, 1])
            else:
                c = random.choice([-1, 1])

            for build in self.all_builds:
                if build.pos_x == animal.pos_x + l and build.pos_y == animal.pos_y + c:
                    m = False
                    if build.name == "enclos" or build.name == "trap":
                        m = True
                    break

            for animalx in self.all_animals:
                if animalx.pos_x == animal.pos_x + l and animalx.pos_y == animal.pos_y + c:
                    m = False
                    break

            if m and 0 < animal.pos_x + l < 6 and 0 < animal.pos_y + c < 6:
                if animal.name != "fairy":
                    self.sound.play(animal.name)
                for button in self.all_buttons:
                    if button.name == "remove":
                        if 320 <= button.rect.x <= 960 and 45 <= button.rect.y <= 690:
                            if self.co_to_pos(button.rect.x - 30, button.rect.y + 30) == (animal.pos_x, animal.pos_y):
                                self.all_buttons.remove(button)
                animal.moving = True
                animal.destination = [l, c]
                animal.pos_x = animal.pos_x + l
                animal.pos_y = animal.pos_y + c
                if l == -1:
                    animal.image = pygame.transform.scale(pygame.image.load(f"assets/animals/left_{animal.name}.png"), (100, 100))
                else:
                    animal.image = pygame.transform.scale(pygame.image.load(f"assets/animals/{animal.name}.png"), (100, 100))
                if self.potions[3] > 0:
                    self.rotate(animal)
                for build in self.all_builds:
                    if build.pos_x == animal.pos_x and build.pos_y == animal.pos_y:
                        if build.name == "enclos":
                            animal.lock = True
                            if animal.name == "beef":
                                build.item = ["leather", "leather", "leather", "leather", "leather", "leather", "raw_beef"]
                            elif animal.name == "lamb":
                                build.item = ["wool", "wool", "wool", "wool", "wool", "wool", "raw_lamb"]
                            elif animal.name == "chicken":
                                build.item = ["eggs", "eggs", "eggs", "eggs", "eggs", "eggs", "raw_chicken"]
                            elif animal.name == "fairy":
                                build.item = ["magic"]
                            if self.all_tips["animal_food_enclos"] == False:
                                self.all_tips["animal_food_enclos"] = True
                                self.reset_tips("utilisez de la nourriture", "pour animaux afin", "de récupérer des ressources")

                        elif build.name == "trap":
                            animal.lock = True
                            build.max_time = 160
                            if animal.name != "fairy":
                                build.item = ["raw_" + animal.name]
                            else:
                                build.item = ["magic"]
                            build.duration = 1
                        break
                break

            else:
                l = 0
                c = 0

    #détecte quand le joueur nourrit un animal
    def feed_animals(self, item, pos_x, pos_y):
        for build in self.all_builds:
            if build.pos_x == pos_x and build.pos_y == pos_y:
                for animal in self.all_animals:
                    if animal.pos_x == build.pos_x and animal.pos_y == build.pos_y:
                        if build.name == "enclos":
                            build.max_time = 310
                            build.food += 1
                            self.remove_item_fast(item)
                            self.shift_item(build.pos_x, build.pos_y)
                            break
                break

    #fait fonctionner le bouton "delete" (EX: pour supprimer un batiment)
    def delete_button(self, button):
        if 320 <= button.rect.x <= 960 and 45 <= button.rect.y <= 690:
            pos_x, pos_y = self.co_to_pos(button.rect.x, button.rect.y)
            for build in self.all_builds:
                if build.pos_x == pos_x and build.pos_y == pos_y:
                    self.all_builds.remove(build)
                    break
            for animal in self.all_animals:
                if animal.pos_x == pos_x and animal.pos_y == pos_y:
                    self.all_animals.remove(animal)
                    break
        elif button.rect.x == 1240:
            self.reset_boat()
        else:
            for pnj in self.all_pnj:
                if pnj.rect.y + 60 <= button.rect.y <= pnj.rect.y + 100:
                    self.all_pnj.remove(pnj)
                    for button in self.all_buttons:
                        if button.name == "remove" and button.rect.x in [1180, 1240]:
                            self.all_buttons.remove(button)
                    self.spawn_remove()
                    self.see_craft()
                    break

    #ouvre l'interface du marché
    def open_market(self):
        self.all_buttons = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.spawn_cross()
        if self.current != 15:
            self.current = 8
        else:
            for index, button in enumerate(self.building):
                self.all_interface_item.add(InterfaceItem(self, index * 80 + 200, 80, button, 50))
        self.interface = 2
        for index, item in enumerate(self.actual_market_trades):
            self.spawn_button("market_trade", math.ceil((index+1)/2) * 350 - 140, index % 2 * 250 + 140, 200)
        i = 0

        for button in self.all_buttons:
            if button.name == "market_trade":
                button.item = self.actual_market_trades[i]
                for trade in self.market_trades:
                    if trade["item"] == button.item:
                        button.price = trade["price"]
                        break
                self.all_interface_item.add(InterfaceItem(self, button.rect.x + 60, button.rect.y + 30, button.item, 80))
                i += 1

    #détecte quand le joueur achète un item
    def buy_item(self, button):
        if self.money >= button.price:
            self.money -= button.price
            self.sound.play("buy")
            if button.item != "boat":
                self.force_spawn_item(button.item)
            i = ""
            if button.item in ["boat", "alembic", "blueprint"]:
                i = button.item
                if button.item == "boat":
                    j = {'item': 'boat', 'price': 25, 'age': 2}
                elif button.item == "alembic":
                    j = {'item': 'alembic', 'price': 30, 'age': 2}
                elif button.item == "blueprint":
                    j = {'item': 'blueprint', 'price': 190, 'age': 3}
            if button.item == i: #permet jsute de retirer le trade du market si on prends un bateau/alembic... le changement d'item est fait avec change_trade_market sinon
                self.all_tips[i] = True
                if i == "boat":
                    self.reset_tips("Le bateau permet", "d'échanger des objets", "à grande échelle")
                elif i == "alembic":
                    self.reset_tips("l'alembic permet de", "fabriquer des potions", "farfeulues")
                self.market_trades.remove(j)
                for trade in self.actual_market_trades:
                    if trade == i:
                        self.actual_market_trades.remove(trade)
                        k = True
                        while k:
                            t = random.choice(self.market_trades)
                            if t["item"] not in self.actual_market_trades and t["age"] <= self.age:
                                k = False
                                self.actual_market_trades.append(t["item"])
                                self.open_market()
            else:
                self.change_trade_market(button)

    #changement des trades du marché (Button=True -> le joueur à acheté, Button=False -> Le changement automatique)
    def change_trade_market(self, button):
        if button == False : # si  c'est le changement automatique des trades
            self.change_market_trade_time = 0
            del self.actual_market_trades[random.randint(0, 5)]
            i = True
            while i:
                t = random.choice(self.market_trades)
                if t["item"] not in self.actual_market_trades and t["age"] <= self.age:
                    self.actual_market_trades.append(t["item"])
                    i = False
            if self.current == 8:
                self.open_market()

        else:  #si c'est pas automatique (que le joueur achète qq chose)
            self.actual_market_trades.remove(button.item)
            i = True
            while i:
                t = random.choice(self.market_trades)
                if t["item"] not in self.actual_market_trades and t["age"] <= self.age:
                    self.actual_market_trades.append(t["item"])
                    i = False
            self.open_market()

    #ouvre l'interface du bateau
    def open_boat(self):
        self.spawn_cross()
        self.interface = 1
        self.current = 11
        self.update_cross_pos()
        for i in range(3):
            self.all_crafts.add(Craft(self, [self.boat_trade[7]], self.boat_trade[i * 2 + 1], "medium", 415, i*100 + 220))

    #détecte quand le joueur échange avec le bateau
    def boat_trading(self, craft):
        for i in range(3):
            if self.boat_trade[i * 2 + 1] == craft.product:
                nb = 0
                for item in self.all_items:
                    if item.name == self.boat_trade[7]:
                        nb += 1
                if nb >= self.boat_trade[i * 2 + 2]:
                    for button in self.all_buttons:
                        if button.name == "remove" and button.rect.x == 1240:
                            self.all_buttons.remove(button)
                            break
                    self.money += craft.product
                    self.cross()
                    for j in range(self.boat_trade[i * 2 + 2]):
                        for item in self.all_items:
                            if item.name == self.boat_trade[7]:
                                self.remove_item_fast(item)
                                break
                    self.reset_boat()
                    for craft in self.all_crafts:
                        if craft.rect.x == 415:
                            self.all_crafts.remove(craft)
                break

    #remet l'image du bateau à 0
    def reset_boat(self):
        self.boat_image = pygame.transform.scale(pygame.image.load("assets/builds/back_boat.png"), (130, 130))
        self.boat_rect.x += 2
        self.boat_time = 1
        i = True
        while i:
            self.boat_trade = random.choice(self.boat_trades)
            if self.boat_trade[0] <= self.age:
                i = False
        for button in self.all_buttons:
            if button.rect.x == 1240 and button.name == "remove":
                self.all_buttons.remove(button)
                break

    #enlève les tips présents à l'écran
    def reset_tips(self, text1, text2, text3):
        self.tips_co_x = 1580
        self.tips_time = 0
        self.is_tips = True
        self.text = text1
        self.text2 = text2
        self.text3 = text3

    #ouvre l'interfac de l'alembic
    def open_alembic(self):
        self.current = 12
        self.interface = 2
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.see_alembic_craft(False)

    #met les crafts de l'alembic à l'écran
    def see_alembic_craft(self, building):
        self.all_crafts = pygame.sprite.Group()
        j = [["flask", "flame", "flame", "enormous_stone", "pickaxe", "log", "log", "magic", "magic"],
             ["flask", "flame", "animals_food", "wool", "leather", "eggs", "magic"],
             ["flask", "flame", "gold", "gold", "gold", "pickaxe", "magic", "magic"],
             ["flask", "flame", "leaves", "leaves", "enormous_stone", "magic"],
             ["flask", "flame",  "magic", "sawdust", "clay", "sand", "sugar", "gigantic_stone", "log", "diamond", "steak"]]
        k = 10
        if building:
            k = -60
        for i in range(len(j)):
            self.all_crafts.add(Craft(self, j[i], str(i + 1), "big", 140, (i + 1) * 90 - k))
        if building == False:
            for craft in self.all_crafts:
                self.all_interface_item.add(InterfaceItem(self, 1060, craft.rect.y + 14, craft.product, 40))
                for index, item in enumerate(craft.recipe):
                    self.all_interface_item.add(InterfaceItem(self, index * 70 + 160, craft.rect.y + 14, item, 40))

    #détecte si le joueur à appuyé sur une potion et affiche le choix de niveau
    def click_on_potion(self, potion):
        self.current = 13
        self.all_crafts = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.all_images_to_draw = []
        j = ["silver", "gold", "diamond"]
        recipe = ""
        for item in potion.recipe:
            recipe += item + " "

        if potion.product == "1" or potion.product == "3" or potion.product == "5":
            for i in range(3):
                self.all_crafts.add(Craft(self, potion.recipe, potion.product, "potion", i * 345 + 195, 250))
        elif potion.product == "2":
            for i in range(2):
                self.all_crafts.add(Craft(self, potion.recipe, potion.product, "potion", i * 460 + 330, 250))
        else:
            self.all_crafts.add(Craft(self, potion.recipe, potion.product, "potion", 540, 250))

        for index, craft in enumerate(self.all_crafts):
            craft.recipe = (recipe + j[index]).split()
            craft.level = f"niveau {index + 1}"
            craft.item = pygame.transform.scale(pygame.image.load(f"assets/items/{j[index]}.png"), (120, 120))

    #gère l'utilisation des potions
    def potion_usage(self):

        if self.potions[2] >= 1:  # permet de faire fonctionner la potion de gold
            self.gold_potion_time -= 1
            if self.gold_potion_time == 0:
                self.money += 1
                self.gold_potion_time = 55

        for index in range(5): # permet d'user les potions (et de les arreter après)
            if self.potions[index] > 0:
                self.potions[index] -= 1
                if self.potions[index] == 0:
                    if index == 0:
                        self.build_speed_potions = 1

                    elif index == 1:
                        self.animal_speed_potion = 1

                    elif index == 2:
                        self.gold_potion_time = 55

                    elif index == 3:
                        for build in self.all_builds:
                            self.rotate(build)
                        for item in self.all_items:
                            self.rotate(item)
                        for animal in self.all_animals:
                            self.rotate(animal)
                        for pnj in self.all_pnj:
                            self.rotate(pnj)
                        for button in self.all_buttons:
                            self.rotate(button)
                        all_images = {}
                        for name, image in self.all_images.items():
                            all_images[name] = pygame.transform.flip(image, False, True)
                        self.all_images = all_images
                        self.boat_image = pygame.transform.flip(self.boat_image, False, True)

                    elif index == 4:
                        self.random_potion_level = 0

    #ouvre l'interface de la cuisine
    def open_kitchen(self):
        self.current = 10
        self.interface = 2
        self.craft_offset = 0
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.see_kitchen_craft()

    #détecte les crafts de la cuisine et les affiche
    def see_kitchen_craft(self):
        self.actual_craft = {}
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.all_crafts = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        items = []
        for item in self.all_items:
            if item.affected == False:
                items.append(item.name)

        for craft in self.kitchen_craft:
            if self.age >= craft["age"]:
                if self.list_in_list(craft["recipe"], items):
                    self.actual_craft[craft["product"]] = craft["recipe"]
                    self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 85 - 20))

        if len(self.actual_craft) > 7 and self.craft_offset + 7 < len(self.actual_craft):
            self.spawn_button("arrow_down", 1220, 385, 50)
            self.arrow_button(10)
        if self.craft_offset > 0:
            self.spawn_button("arrow_up", 1220, 305, 50)
            self.arrow_button(10)
        else:
            self.set_image_to_craft()

    #ouvre l'interface de la meule
    def open_meule(self):
        self.current = 14
        self.interface = 2
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.see_meule_craft()

    #détecte les crafts de la meule et les affiche
    def see_meule_craft(self):
        self.actual_craft = {}
        self.all_crafts = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.all_images_to_draw = []
        items = []
        for item in self.all_items:
            if item.affected == False:
                items.append(item.name)

        for craft in self.meule_craft:
            if self.age >= craft["age"]:
                if craft["recipe"] in items:
                    self.actual_craft[craft["recipe"]] = craft["product"]
                    self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "meule", (len(self.actual_craft) + 1) % 2 * 500 + 200, math.ceil(len(self.actual_craft) / 2) * 87 + 55))

        for craft in self.all_crafts:
            self.all_images_to_draw.append(self.all_images[craft.recipe])
            self.all_images_to_draw.append((craft.rect.x + 20, craft.rect.y + 15))
            for index, item in enumerate(craft.product):
                self.all_images_to_draw.append(self.all_images[item])
                self.all_images_to_draw.append((craft.rect.x + index * 55 + 130, craft.rect.y + 15))

    #ouvre l'interface du building
    def open_building(self):
        self.current = 15
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.interface = 2
        self.see_building_crafts()

    #détecte les crafts du building en fonction de l'interface sélectionnée. (EX: si le joueur est sur l'interface de la cuisne alors il détecte les crafts de la cuisine)
    def see_building_crafts(self):
        self.actual_craft = {}
        self.all_crafts = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.all_buttons = pygame.sprite.Group()
        self.spawn_cross()
        self.all_images_to_draw = []
        for index, button in enumerate(self.building):
            self.all_interface_item.add(InterfaceItem(self, index * 80 + 200, 80, button, 50))

        items = []
        for item in self.all_items:
            items.append(item.name)

        if self.building[self.selected_builder - 1] == "big_craft_table":
            for craft in self.big_crafts:
                if craft["age"] <= self.age:
                    if self.list_in_list(craft["recipe"], items):
                        self.actual_craft[craft["product"]] = craft["recipe"]
                        self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 87 + 55))

        elif self.building[self.selected_builder - 1] == "kitchen":
            for craft in self.kitchen_craft:
                if craft["age"] <= self.age:
                    if self.list_in_list(craft["recipe"], items):
                        self.actual_craft[craft["product"]] = craft["recipe"]
                        self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "big", 140, len(self.actual_craft) * 87 + 55))

        elif self.building[self.selected_builder - 1] == "meule":
            for craft in self.meule_craft:
                if craft["age"] <= self.age:
                    if craft["recipe"] in items:
                        self.actual_craft[craft["recipe"]] = craft["product"]
                        self.all_crafts.add(Craft(self, craft["recipe"], craft["product"], "meule", (len(self.actual_craft) + 1) % 2 * 500 + 200, math.ceil(len(self.actual_craft) / 2) * 87 + 55))

        elif self.building[self.selected_builder - 1] == "alembic":
            self.see_alembic_craft(True)

        elif self.building[self.selected_builder - 1] == "market":
            for index, item in enumerate(self.actual_market_trades):
                self.spawn_button("market_trade", math.ceil((index + 1) / 2) * 350 - 140, index % 2 * 250 + 140, 200)

        elif self.building[self.selected_builder - 1] == "pass":
            i = [570, 474, 378, 282, 146, 146, 242, 338, 434, 530]
            for k in range(25):
                j = 90 #taille du bouton
                l = 200 # potision en x
                if (k + 1) % 5 == 0:
                    j = 130
                    l = 180
                p = math.ceil((k + 1) / 5) * l #position finale
                if (k + 1) % 5 == 0:
                    p += math.floor(k / 5) * 20
                self.spawn_button('market_trade', p - 10, i[k % 10], j)
                for button in self.all_buttons:
                    if button.rect.x == p - 10 and button.rect.y == i[k % 10]:
                        button.item = self.pass_trades[k][1]
                        button.price = self.pass_trades[k][0]

        if self.building[self.selected_builder - 1] == "meule":
            for craft in self.all_crafts:
                self.all_images_to_draw.append(self.all_images[craft.recipe])
                self.all_images_to_draw.append((craft.rect.x + 20, craft.rect.y + 15))
                for index, item in enumerate(craft.product):
                    self.all_images_to_draw.append(self.all_images[item])
                    self.all_images_to_draw.append((craft.rect.x + index * 55 + 130, craft.rect.y + 15))

        elif self.building[self.selected_builder - 1] == "market":
            i = 0
            for button in self.all_buttons:
                if button.name == "market_trade":
                    button.item = self.actual_market_trades[i]
                    for trade in self.market_trades:
                        if trade["item"] == button.item:
                            button.price = trade["price"]
                            break
                    self.all_interface_item.add(InterfaceItem(self, button.rect.x + 60, button.rect.y + 30, button.item, 80))
                    i += 1

        elif self.building[self.selected_builder - 1] == "pass":
            n = 0
            for button in self.all_buttons:
                if button.name == "market_trade":
                    n += 1
                    if n < self.pass_number:
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/checked.png"), (20, 20)))
                        self.all_images_to_draw.append([button.rect.x + button.scale - 30, button.rect.y + 10])
                        if len(button.item) == 1:

                            if button.item[0][0] == "1" or button.item[0][0] == "5":
                                self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/builds/{button.item[0][0]}.png"), (50, 50)))

                            elif button.item[0] == "cave":
                                self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/builds/{button.item[0]}.png"),(50, 50)))
                            else:
                                self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{button.item[0]}.png"), (50, 50)))
                            self.all_images_to_draw.append([button.rect.x + button.scale / 2 - 25, button.rect.y + button.scale / 2 - 25])

                        elif button.scale == 90:
                            for index, item in enumerate(button.item):
                                k = 11  # position en x
                                if (index + 1) % 2 == 0:
                                    k = 49
                                l = 11  # offset en y
                                if len(button.item) < 3:
                                    l = 30
                                self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{item}.png"), (30, 30)))
                                self.all_images_to_draw.append([button.rect.x + k, button.rect.y + math.floor(index / 2) * 38 + l])

                        else:
                            for index, item in enumerate(button.item):
                                k = 15
                                if (index + 1) % 2 == 0:
                                    k = 75
                                l = 15  # offset en y
                                if len(button.item) < 3:
                                    l = 45
                                self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{item}.png"), (40, 40)))
                                self.all_images_to_draw.append([button.rect.x + k, button.rect.y + math.floor(index / 2) * 60 + l])
                    else:
                        break

        else:
            if len(self.actual_craft) > 6 and self.craft_offset + 6 < len(self.actual_craft):
                self.spawn_button("arrow_down", 1220, 385, 50)
                self.arrow_button(12)
            if self.craft_offset > 0:
                self.spawn_button("arrow_up", 1220, 305, 50)
                self.arrow_button(12)
            else:
                self.set_image_to_craft()

    #fait une rotation à 180° de tous les objets (utilisé pour la potion qui met à l'envers)
    def rotate(self, sprite):
        sprite.rect = sprite.image.get_rect(center=sprite.rect.center)
        sprite.image = pygame.transform.rotozoom(sprite.image, 180, 1)

    #fait apparaitre tous les boutons "remove" (au dessus des PNJ et à coté du bateau)
    def spawn_remove(self):
        for pnj in self.all_pnj:
            self.spawn_button("remove", 1180, pnj.rect.y + 60, 30)
        if self.all_tips["boat"] == True and self.boat_time == 0:
            self.spawn_button("remove", 1240, 650, 30)

    #permet de décaller les crafts quand on clique sur une flèche
    def arrow_button(self, nb): # nb = l'interface dans laquelle on est
        if nb == 0:
            for craft in self.all_crafts:
                if craft.rect.x == 15:
                    self.all_crafts.remove(craft)
        else:
            self.all_crafts = pygame.sprite.Group()
        if nb == 0:
            for index, craft in enumerate(self.actual_craft.items()):
                self.all_crafts.add(Craft(self, craft[1], craft[0], "", 15, 0))
        elif nb == 1:
            for index, craft in enumerate(self.actual_craft.items()):
                self.all_crafts.add(Craft(self, craft[1], craft[0], "", 350 * (index % 3) + 150, 0))
        elif nb == 2 or nb == 4 or nb == 10 or nb == 11 or nb == 12:
            for index, craft in enumerate(self.actual_craft.items()):
                self.all_crafts.add(Craft(self, craft[1], craft[0], "big", 140, 0))
        k = 0 # le nombre de craft qu'il y a. PERMET DE RETIRER LES CRAFT DES PNJ (par exemple)
        i = 0 #le nombre de craft à l'écran
        if nb == 0:
            for craft in self.all_crafts:
                if craft.rect.x == 15:
                    k += 1
                    if self.craft_offset < k < self.craft_offset + 8:
                        i += 1
                        craft.rect.y = i * 85 - 25
                    else:
                        self.all_crafts.remove(craft)
        elif nb == 1:
            for craft in self.all_crafts:
                k += 1
                if self.craft_offset < k < self.craft_offset + 19:
                    i += 1
                    craft.rect.y = math.ceil((i) / 3) * 85 + 65
                else:
                    self.all_crafts.remove(craft)
            self.set_image_to_craft()

        elif nb == 2 or nb == 4 or nb == 12:
            for craft in self.all_crafts:
                k += 1
                if self.craft_offset < k < self.craft_offset + 7:
                    i += 1
                    craft.rect.y = i * 85 + 65
                else:
                    self.all_crafts.remove(craft)
            self.set_image_to_craft()

        elif nb == 10 or nb == 11:
            for craft in self.all_crafts:
                k += 1
                if self.craft_offset < k < self.craft_offset + 8:
                    i += 1
                    craft.rect.y = i * 85 - 10
                else:
                    self.all_crafts.remove(craft)
            self.set_image_to_craft()

    #met du carburant dans la fusée
    def fusee_oil(self, item, pos_x, pos_y):
        for build in self.all_builds:
            if build.pos_x == pos_x and build.pos_y == pos_y:
                if build.name == "launch_pad_on" and self.fusee_rect_y == self.fusee_pad_rect_y:
                    self.fusee_fuel += 1
                    self.remove_item_fast(item)
                    if self.fusee_fuel == 5:
                        build.image = pygame.transform.scale(pygame.image.load(f"assets/builds/launch_pad_off.png"), (100, 100))
                        self.fusee_fuel -= 5
                        self.fusee_time = 1800
                        self.sound.play("fusée")
                    break
                break

    #affects une étoile à un batiment
    def affect_star(self, item, pos_x, pos_y):
        for build in self.all_builds:
            if build.pos_x == pos_x and build.pos_y == pos_y:
                if build.item != [] and build.star < 6:
                    build.star += 1
                    self.sound.play("bonus")
                    self.remove_item_fast(item)
                break

    #construit un hopper
    def build_hopper(self, co, item):

        x, y = self.co_to_pos(co[0], co[1])
        i = True
        j = 0
        for hopper in self.all_hopper:
            if hopper.pos_x + hopper.direction[0] == x and hopper.pos_y + hopper.direction[1] == y:
                j += 1
        if j == 4:
            i = False
        for hopper in self.all_hopper:
            if hopper.pos_x == x and hopper.pos_y == y:
                i = False
        if i:
            self.all_hopper.add(Hopper(self, x, y, [0, -1]))
            self.shift_item(x, y)
            self.all_items.remove(item)
            return True

    #avance d'une case dans la roue de la chance (pass=roue de la chance)
    def buy_pass(self, button):
        j = False
        if button.price[0].isdigit():
            if self.money >= int(button.price[0]):
                self.money -= int(button.price[0])
                j = True

        else:
            i = []
            items = []
            for item in button.price:
                i.append(item)

            for item in self.all_items:
                items.append(item.name)

            if self.list_in_list(i, items):
                for item in button.price:
                    for itemx in self.all_items:
                        if itemx.name == item:
                            self.remove_item_fast(itemx)
                            break
                j = True
        if j:
            self.sound.play("buy")
            self.pass_number += 1
            t = False
            for item in button.item:
                if item == "trophy":
                    self.new_age()
                    t = True
                elif item == "remove_sell":
                    self.delete_sell = True
                    self.cross()
                    self.reset_tips("Détruire les objets", "vous rapporte maintenant", "de l'argent")
                else:
                    self.force_spawn_item(item)
                    self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/checked.png"), (20, 20)))
                    self.all_images_to_draw.append([button.rect.x + button.scale - 30, button.rect.y + 10])

            if t == False:
                if len(button.item) == 1:
                    if button.item[0][0] == "1" or button.item[0][0] == "5":
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/builds/{button.item[0][0]}.png"), (50, 50)))
                    elif button.item[0] == "cave":
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/builds/{button.item[0]}.png"), (50, 50)))
                    else:
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{button.item[0]}.png"), (50, 50)))
                    self.all_images_to_draw.append([button.rect.x + button.scale/2 - 25, button.rect.y + button.scale/2 - 25])

                elif button.scale == 90:
                    for index, item in enumerate(button.item):
                        k = 11 #position en x
                        if (index + 1) % 2 == 0:
                            k = 49
                        l = 11 #offset en y
                        if len(button.item) < 3:
                            l = 30
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{item}.png"), (30, 30)))
                        self.all_images_to_draw.append([button.rect.x + k, button.rect.y + math.floor(index / 2) * 38 + l])

                else:
                    for index, item in enumerate(button.item):
                        k = 15
                        if (index + 1) % 2 == 0:
                            k = 75
                        l = 15  # offset en y
                        if len(button.item) < 3:
                            l = 45
                        self.all_images_to_draw.append(pygame.transform.scale(pygame.image.load(f"assets/items/{item}.png"), (40, 40)))
                        self.all_images_to_draw.append([button.rect.x + k, button.rect.y + math.floor(index / 2) * 60 + l])

    #permet de passer au nouvel age
    def new_age(self):
        self.current = 5
        self.interface = 1
        self.all_buttons = pygame.sprite.Group()
        self.all_interface_item = pygame.sprite.Group()
        self.all_images_to_draw = []
        self.spawn_cross()
        self.update_cross_pos()
        self.age += 1
        self.is_tips = False
        self.tips_co_x = 1280
        if self.age == 2:
            self.text3 = "Commerce et Agriculture"
            self.pnj_time = 700
            self.animal_time = 800
        elif self.age == 3:
            self.text3 = "Magie et Alchimie"
        elif self.age == 4:
            self.text3 = "l'Industrialisation"
            self.market_trades.append({'item': 'blueprint', 'price': 190, 'age': 3})
            self.potion_random[2].append("star")
            self.potion_random[2].append("shard")
            self.potion_random[2].append("plate")
            self.all_tips["blueprint"] = False
        self.text = "Felicitations"
        self.text2 = f"Vous passez à l'age {self.age},"
        if self.age == 5:
            self.age = 4
            self.text = "BRAVO !"
            self.text2 = "Vous avez finit les 4 ages !"
            self.text3 = "Merci d'avoir joué !"

    #sauvegarde le jeu
    def save_game(self):
        with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/name_pos.txt", "w+") as file:
            with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/produce_item.txt", "w+") as file2:
                with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/chest_contenu.txt", "w+") as file3:
                    with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/fire_fuel.txt", "w+") as file5:
                        with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/classic_inventory.txt", "w+") as file6:
                            for build in self.all_builds:
                                if build.special == "":
                                    build.special = "None"
                                if build.can_affect == None:
                                    build.can_affect = "None"
                                if build.affected_object == "":
                                    build.affected_object = "None"
                                file.write(build.name + " " + str(build.pos_x) + " " + str(build.pos_y) + " " + str(build.time) + " " + str(build.max_time) + " " + str(build.affected_object) + " " + build.special + " " + str(build.duration) + " " + build.can_affect + " " + str(build.star) + "\n")

                                line = ""
                                for item in build.item:
                                    line += f"{item} "
                                file2.write(line + "\n")

                                if build.name == "chest":
                                    line = str(build.pos_x) + " " + str(build.pos_y) + " "
                                    for item, value in build.chest_item.items():
                                        line += f"{item} {value} "
                                    file3.write(line + "\n")

                                elif build.name == "fire" or build.name == "kitchen" or build.name == "building":
                                    file5.write(str(build.pos_x) + " " + str(build.pos_y) + " " + str(build.fuel) + "\n")

                                elif build.name == "enclos":
                                    file5.write(str(build.pos_x) + " " + str(build.pos_y) + " " + str(build.food) + "\n")

                                elif build.name == "treasure":
                                    i = ""
                                    for item in build.items_in:
                                        i += item + " "
                                    file6.write(str(build.pos_x) + " " + str(build.pos_y) + " " + i + "\n")

                            file6.close()
                        file5.close()
                    file3.close()
                file2.close()
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/items/name_pos.txt", "w+") as file:
            for item in self.all_items:
                file.write(item.name + " " + str(item.pos_x) + " " + str(item.pos_y) + " " + str(item.affected) + "\n")
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/infos.txt", "w+") as file:
            file.write(str(self.age) + "\n")
            file.write(f"{self.boat_trade[7]} {self.boat_rect.x} {self.boat_time}" + "\n")
            for value in self.all_tips.values():
                file.write(f"{value} ")
            file.write(f"\n{self.money}")

            i = ""
            for builder in self.building:
                i += f"{builder} "
            file.write(f"\n{i}")

            i = ""
            for potion in self.potions:
                i += f"{potion} "
            file.write(f"\n{i}")

            file.write(f"\n{self.build_speed_potions} {self.animal_speed_potion} {self.gold_potion_time} {self.random_potion_level}")

            i = ""
            for trade in self.actual_market_trades:
                i += f"{trade} "
            file.write(f"\n{i}")

            file.write(f"\n{self.pass_number}")

            file.write(f"\n{self.fusee_rect_x} {self.fusee_rect_y} {self.fusee_pad_rect_y} {self.fusee_time} {self.fusee_fuel}")

            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/animals.txt", "w+") as file:
            for animal in self.all_animals:
                file.write(animal.name + " " + f"{animal.pos_x} {animal.pos_y} {animal.lock} {animal.time} {animal.rect.x} {animal.rect.y} {animal.moving} {animal.destination[0]} {animal.destination[1]}\n")
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/pnj.txt", "w+") as file:
            for pnj in self.all_pnj:
                i = ""
                for item in pnj.recipe:
                    i += item + " "
                file.write(f"{pnj.name} {pnj.rect.y} {pnj.product} {i} \n")
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/hopper.txt", "w+") as file:
            for hopper in self.all_hopper:
                file.write(f"{hopper.pos_x} {hopper.pos_y} {hopper.direction[0]} {hopper.direction[1]}\n")
            file.close()

    #charge une sauvegarde du jeu
    def load_game(self):
        self.all_buttons = pygame.sprite.Group()
        self.spawn_button("delete_off", 100, 10, 50)
        self.spawn_button("build_helper_button", 30, 10, 50)
        self.writing = False
        self.saves_names = os.listdir("saves")
        self.all_builds = pygame.sprite.Group()
        self.current = 1
        with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/name_pos.txt", "r+") as file:
            with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/produce_item.txt", "r+") as file2:
                with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/chest_contenu.txt", "r+") as file3:
                    with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/fire_fuel.txt", "r+") as file5:
                        with open(f"saves/{self.saves_names[self.selected_save - 1]}/builds/classic_inventory.txt", "r+") as file6:
                            f1 = []
                            f2 = []
                            f3 = []
                            f5 = []
                            f6 = []
                            for line in file:
                                f1.append(line.split())
                            for line in file2:
                                f2.append(line.split())
                            for line in file3:
                                f3.append(line.split())
                            for line in file5:
                                f5.append(line.split())
                            for line in file6:
                                f6.append(line.split())

                            for i in range(len(f1)):
                                j = f1[i][6]
                                if j == "None":
                                    j = ""
                                self.all_builds.add(Build(self, f1[i][0], int(f1[i][1]), int(f1[i][2]), False, j))

                                for build in self.all_builds:
                                    if build.pos_x == int(f1[i][1]) and build.pos_y == int(f1[i][2]):
                                        build.time = int(f1[i][3])

                                        if f1[i][5] != "None":
                                            build.affected_object = f1[i][5]

                                        if f1[i][4] != "None":
                                            build.max_time = int(f1[i][4])

                                        if f1[i][8] != "None":
                                            build.can_affect = f1[i][8]

                                        if f1[i][7] != "None":
                                            build.duration = int(f1[i][7])

                                        build.item = f2[i]
                                        build.can_affect = f1[i][8]
                                        build.star = int(f1[i][9])

                                        if build.name == "chest":
                                            build.chest_item = {}
                                            for chest_info in f3:
                                                if build.pos_x == int(chest_info[0]) and build.pos_y == int(chest_info[1]):
                                                    for j in range(int((len(chest_info) - 2) / 2)):
                                                        build.chest_item[chest_info[j*2 + 2]] = int(chest_info[j*2 + 3])
                                                    break

                                        elif build.name == "fire" or build.name == "enclos" or build.name == "kitchen" or build.name == "building":
                                            for fire_info in f5:
                                                if int(fire_info[0]) == build.pos_x and int(fire_info[1]) == build.pos_y:
                                                    if build.name != "enclos":
                                                        build.fuel = int(fire_info[2])
                                                    else:
                                                        build.food = int(fire_info[2])
                                                    break

                                        elif build.name == "boat_crate" or build.name == "treasure":
                                            for info in f6:
                                                if int(info[0]) == build.pos_x and int(info[1]) == build.pos_y:
                                                    build.items_in = []
                                                    for j in range(len(info) - 2):
                                                        build.items_in.append(info[j + 2])
                                                    break

                                        elif build.name == "market":
                                            for info in f6:
                                                if int(info[0]) == build.pos_x and int(info[1]) == build.pos_y:
                                                    build.trades = []
                                                    build.change_trades_time = 0
                                                    for i in range(6):
                                                        build.trades.append(info[i + 2])
                                                    break

                            file6.close()
                        file5.close()
                    file3.close()
                file2.close()
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/items/name_pos.txt", "r+") as file:
            for line in file:
                ligne = line.split()
                i = True
                if ligne[3] == "False":
                    i = False
                self.spawn_item(ligne[0], int(ligne[1]), int(ligne[2]), i)
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/infos.txt", "r+") as file:
            info_list = []
            for line in file:
                info_list.append(line)
            self.age = int(info_list[0])
            info_list[1] = info_list[1].split()

            for trade in self.boat_trades:
                if trade[7] == info_list[1][0]:
                    self.boat_trade = trade
            self.boat_rect.x = int(info_list[1][1])
            self.boat_time = int(info_list[1][2])

            self.money = int(info_list[3])
            info_list[2] = info_list[2].split()
            for index, info in enumerate(info_list[2]):
                if info == "False":
                    info_list[2][index] = False
                else:
                    info_list[2][index] = True
                    if index == 0:
                        self.market_trades.remove({"item": "boat", "price": 25, "age": 2})
                    elif index == 5:
                        self.big_crafts.remove({'recipe': ['planks', 'planks', 'planks', 'planks', 'apple', 'apple', 'gigantic_stone', 'gold'], 'product': 'market', 'age': 2})
                    elif index == 8:
                        self.market_trades.remove({'item': 'alembic', 'price': 30, 'age': 2})
                    elif index == 9:
                        self.market_trades.remove({'item': 'blueprint', 'price': 190, 'age': 3})
                    elif index == 10:
                        self.big_crafts.remove({'recipe': ['t1', 't1', 't1', 't1', 't1'], 'product': 's1wheat', 'age': 2})
                    elif index == 11:
                        self.big_crafts.remove({'recipe': ['brick', 'brick', 'brick', 'brick', 'glass', 'glass', 'glass', 'glass', 'blueprint'], 'product': 'building', 'age': 3})
                        self.is_building = True
                    elif index == 12:
                        self.big_crafts.remove({'recipe': ["s1wheat", "t1", "t1", "t1", "flame", "flame", "flame"], 'product': 't1apple', 'age': 2})
            for index, key in enumerate(self.all_tips.keys()):
                self.all_tips[key] = info_list[2][index]

            info_list[4] = info_list[4].split()
            for build in info_list[4]:
                self.building.append(build)

            info_list[5] = info_list[5].split()
            for index, potion in enumerate(info_list[5]):
                self.potions[index] = int(potion)

            info_list[6] = info_list[6].split()
            self.build_speed_potions = int(info_list[6][0])
            self.animal_speed_potions = int(info_list[6][1])

            if self.potions[3] >= 1:
                for build in self.all_builds:
                    self.rotate(build)
                for item in self.all_items:
                    self.rotate(item)

            self.gold_potion_time = int(info_list[6][2])
            self.random_potion_level = int(info_list[6][3])

            self.pass_number = int(info_list[8])

            info_list[9] = info_list[9].split()

            self.fusee_rect_x = int(info_list[9][0])
            self.fusee_rect_y = int(info_list[9][1])
            self.fusee_pad_rect_y = int(info_list[9][2])
            self.fusee_time = int(info_list[9][3])
            self.fusee_fuel = int(info_list[9][4])

            if self.fusee_pad_rect_y != self.fusee_rect_y:
                for build in self.all_builds:
                    if build.name == "launch_pad_on":
                        build.image = pygame.transform.scale(pygame.image.load(f"assets/builds/launch_pad_off.png"), (100, 100))
                        break

            if self.pass_number > 10:
                self.delete_sell = True

            if self.all_tips["market"] == True:
                info_list[7] = info_list[7].split()
                for trade in info_list[7]:
                    self.actual_market_trades.append(trade)
            file.close()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/pnj.txt", "r+") as file:
            info_list = []
            for line in file:
                info_list.append(line.split())
            for info in info_list:
                self.all_pnj.add(Pnj(self, info[0]))
                for pnj in self.all_pnj:
                    if pnj.name == info[0]:
                        pnj.rect.x = 1050
                        pnj.rect.y = int(info[1])
                        try:
                            pnj.product = int(info[2])
                        except ValueError:
                            pnj.product = info[2]
                        pnj.recipe = []
                        for i in range(len(info) - 3):
                            pnj.recipe.append(info[i + 3])
                self.see_craft()

        self.spawn_remove()

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/animals.txt", "r+") as file:
            info_list = []
            for line in file:
                info_list.append(line.split())
            for info in info_list:
                info[1] = int(info[1])
                info[2] = int(info[2])
                info[4] = int(info[4])
                info[5] = int(info[5])
                info[6] = int(info[6])
                info[8] = int(info[8])
                info[9] = int(info[9])
                for i in [3, 7]:
                    if info[i] == "False":
                        info[i] = False
                    else:
                        info[i] = True
            for info in info_list:
                self.all_animals.add(Animal(self, info[0], info[1], info[2]))
                for animal in self.all_animals:
                    if animal.pos_x == info[1] and animal.pos_y == info[2]:
                        animal.lock = info[3]
                        animal.time = info[4]
                        animal.rect.x = info[5]
                        animal.rect.y = info[6]
                        animal.moving = info[7]
                        animal.destination = [info[8], info[9]]
                        if animal.moving:
                            if animal.destination[0] == -1:
                                animal.image = pygame.transform.scale(pygame.image.load(f"assets/animals/left_{info[0]}.png"), (100, 100))

        with open(f"saves/{self.saves_names[self.selected_save - 1]}/hopper.txt", "r+") as file:
            for line in file:
                line = line.split()
                self.all_hopper.add(Hopper(self, int(line[0]), int(line[1]), [int(line[2]), int(line[3])]))
            file.close()

    #crée une nouvelle sauvegarde
    def new_game(self):
        i = "New game"
        while os.path.exists(f"saves/{i}"):
            i += " Bis"
        if len(os.listdir("saves")) < 3:
            self.current = 1
            self.all_buttons = pygame.sprite.Group()
            self.spawn_button("delete_off", 100, 10, 50)
            self.spawn_button("build_helper_button", 30, 10, 50)
            self.is_running = True
            os.mkdir(f"saves/{i}")
            os.mkdir(f"saves/{i}/builds")
            os.mkdir(f"saves/{i}/items")

            open(f"saves/{i}/infos.txt", "x")

            open(f"saves/{i}/builds/fire_fuel.txt", "x")
            open(f"saves/{i}/builds/name_pos.txt", "x")
            open(f"saves/{i}/builds/produce_item.txt", "x")
            open(f"saves/{i}/builds/duration.txt", "x")

            open(f"saves/{i}/builds/chest_contenu.txt", "x")
            open(f"saves/{i}/builds/classic_inventory.txt", "x")

            open(f"saves/{i}/items/name_pos.txt", "x")

            open(f"saves/{i}/animals.txt", "x")

            open(f"saves/{i}/pnj.txt", "x")

            open(f"saves/{i}/hopper.txt", "x")

            self.saves_names = os.listdir("saves")
            self.selected_save = self.saves_names.index(i) + 1
