import os
import random

import pygame
import shutil
import math
from game import Game

pygame.init()
game = Game()

if not os.path.exists("saves"):
    os.mkdir("saves")

#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    # Le fichier main gère le déroulement du jeu. Il indique au fichier "game.py" dans quel ordre exécuter les fonctions.
    # Le fichier "game.py" est principalement composé de fonctions. C'est là que le code sera le plus intéressant.
    # Le fichier game fait le lien entre tous les fichiers (items, builds...) et permet leur mise en action.

#|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


clock = pygame.time.Clock()
FPS = 20

pygame.display.set_caption("Four Ages")
screen = pygame.display.set_mode((1280, 720))

game.sound.play("music")

grid = pygame.image.load("assets/grid.png")

back_bar = pygame.image.load("assets/back_bar3.png")
back_bar = pygame.transform.scale(back_bar, (130, 16))

age_ribbon = pygame.image.load("assets/ribbon.png")
age_ribbon = pygame.transform.scale(age_ribbon, (500, 126))

interface = pygame.image.load("assets/interface.png")
little_interface = pygame.image.load("assets/little interface.png")

new_game_button = pygame.image.load("assets/new game button test.png")
new_game_button = pygame.transform.scale(new_game_button, (725, 325))
new_game_button_rect = new_game_button.get_rect()
new_game_button_rect.x = 310
new_game_button_rect.y = 50

saves_button = pygame.image.load("assets/saves_button.png")
saves_button = pygame.transform.scale(saves_button, (300, 123))
saves_button_rect = saves_button.get_rect()
saves_button_rect.x = 490
saves_button_rect.y = 400

builder_selectionner = pygame.image.load("assets/builder_selectionner.png")
builder_selectionner = pygame.transform.scale(builder_selectionner, (70, 70))

item_selectionner = pygame.image.load("assets/item_selectionner.png")
item_selectionner = pygame.transform.scale(item_selectionner, (40, 40))

coin_image = pygame.image.load("assets/items/coin.png")
coin_image = pygame.transform.scale(coin_image, (30, 30))
little_coin_image = pygame.transform.scale(pygame.image.load("assets/items/coin.png"), (15, 15))

coin_interface = pygame.image.load("assets/coin interface.png")
star_image = pygame.transform.scale(pygame.image.load("assets/items/star.png"), (20, 20))
bar_path = pygame.image.load("assets/bar.png")
horizontal_bar_path = pygame.image.load("assets/h_bar.png")

i = 0
running = True
while running:

    screen.blit(game.bg, (0, 0))

    if game.is_running:
        if game.interface != 0:
            if game.interface == 2:
                screen.blit(interface, (70, 30))

            if game.age >= 4 and game.current == 15:
                if game.building[game.selected_builder - 1] == "pass":
                    for i in range(5):
                        screen.blit(bar_path, ((i + 1) * 200 + 25, 146))
                    for i in range(4):
                        screen.blit(horizontal_bar_path, ((i + 1) * 200 + 25, 180 + math.ceil(i % 2) * 420))
            game.all_buttons.draw(screen)

            if game.current == 2:
                game.blit_chest_number(screen)

            elif game.current == 3 or game.current == 12 or game.current == 10 or game.current == 14:
                game.all_crafts.draw(screen)

            elif game.current == 4:
                game.all_crafts.draw(screen)
                screen.blit(builder_selectionner, (game.selected_builder * 80 + 110, 70))

            elif game.current == 8:
                for button in game.all_buttons:
                    if button.name == "market_trade":
                        screen.blit(game.font.render(str(button.price), 1, (100, 20, 20)), (button.rect.x + 110 - len(str(button.price)) * 20, button.rect.y + 120))
                        screen.blit(coin_image, (button.rect.x + 120, button.rect.y + 145))

            elif game.current == 15:
                game.all_crafts.draw(screen)
                screen.blit(builder_selectionner, (game.selected_builder * 80 + 110, 70))

                if game.building[game.selected_builder - 1] == "kitchen":
                    for build in game.all_builds:
                        if build.name == "building":
                            screen.blit(game.little_font.render(f"flamme : {build.fuel}", 1, (100, 20, 20)), (1060, 70))
                            break

                elif game.building[game.selected_builder - 1] == "market":
                    for button in game.all_buttons:
                        if button.name == "market_trade":
                            screen.blit(game.font.render(str(button.price), 1, (100, 20, 20)), (button.rect.x + 110 - len(str(button.price)) * 20, button.rect.y + 120))
                            screen.blit(coin_image, (button.rect.x + 120, button.rect.y + 145))

                elif game.building[game.selected_builder - 1] == "pass" and game.pass_number != 26:
                    i = 0 #compteur de boutons
                    for button in game.all_buttons:
                        if button.name == "market_trade":
                            i += 1
                            if i == game.pass_number:
                                if button.price[0].isdigit():
                                    if len(button.price[0]) == 1:
                                        screen.blit(game.font.render(str(button.price[0]), 1, (100, 20, 20)), (button.rect.x + 20, button.rect.y + 5))
                                        screen.blit(coin_image, (button.rect.x + 40, button.rect.y + 30))
                                    elif len(button.price[0]) == 2:
                                        screen.blit(game.font.render(str(button.price[0]), 1, (100, 20, 20)), (button.rect.x + 10, button.rect.y + 5))
                                        screen.blit(coin_image, (button.rect.x + 50, button.rect.y + 30))
                                    else:
                                        screen.blit(game.little_font.render(str(button.price[0]), 1, (100, 20, 20)), (button.rect.x + 150 - len(str(button.price)) * 20, button.rect.y + 23))
                                        screen.blit(coin_image, (button.rect.x + 50, button.rect.y + 30))
                                else:
                                    for index, item in enumerate(button.price):
                                        j = 0
                                        if (index + 1) % 2 == 0:
                                            j = 1
                                        screen.blit(game.all_images[item], (button.rect.x + 18 + 50*j, button.rect.y + 18 + 50 * (math.ceil((index + 1)/2) - 1)))

            elif game.current == 13:
                game.all_crafts.draw(screen)
                for craft in game.all_crafts:
                    screen.blit(game.font.render(craft.level, 1, (100, 20, 20)), (craft.rect.x + 40, craft.rect.y + 110))
                    screen.blit(craft.item, (craft.rect.x + 40, craft.rect.y + 20))

            for i in range(int(len(game.all_images_to_draw) / 2)):
                screen.blit(game.all_images_to_draw[i * 2], game.all_images_to_draw[i * 2 + 1])
            game.all_interface_item.draw(screen)
        #
        #
        #
        if game.interface != 2:
            screen.blit(grid, (310, 30))
            game.all_animals.draw(screen)
            game.all_builds.draw(screen)
            for build in game.all_builds:
                for i in range(build.star):
                    screen.blit(star_image, (build.rect.x + 110 - ((i + 1) * 24) + (math.ceil((i + 1) / 3) - 1) * 72, (math.ceil((i + 1) / 3) - 1) * 24 + build.rect.y - 10))
                if build.max_time != None:
                    build.update_bar(screen)
                    screen.blit(back_bar, (build.rect.x - 14, build.rect.y + 90))
                if build.name == "fire":
                    screen.blit(game.font.render(str(build.fuel), 1, (100, 20, 20)), (build.rect.x + 75, build.rect.y))
                elif build.name == "enclos":
                    for animal in game.all_animals:
                        if animal.pos_x == build.pos_x and animal.pos_y == build.pos_y:
                            screen.blit(game.font.render(str(build.food), 1, (100, 20, 20)), (build.rect.x + 25, build.rect.y - 35))
                            break
                elif build.name == "kitchen":
                    screen.blit(game.font.render(str(build.fuel), 1, (100, 20, 20)), (build.rect.x + 65, build.rect.y + 25))

                elif build.name == "launch_pad_on" or build.name == "launch_pad_off":
                    screen.blit(game.font.render(str(game.fusee_fuel), 1, (100, 20, 20)), (build.rect.x + 60, build.rect.y - 35))

            for item in game.all_items:
                if item.can_move == True:
                    screen.blit(item_selectionner, (item.rect.x, item.rect.y))
                    break

            for item in game.all_items:
                screen.blit(item.image, (item.rect.x, item.rect.y))
            game.all_pnj.draw(screen)
            game.all_hopper.draw(screen)
            if game.fusee_rect_y != game.fusee_pad_rect_y:
                screen.blit(game.fusee_image, (game.fusee_rect_x, game.fusee_rect_y))

            if game.interface == 1:
                screen.blit(little_interface, (340, 187))
            i = 0
            for craft in game.all_crafts:
                if craft.rect.x == 980 or craft.rect.x == 415:
                    screen.blit(craft.image, craft.rect)
                elif i < 7:
                    i += 1
                    screen.blit(craft.image, craft.rect)
                else:
                    game.all_crafts.remove(craft)

            for craft in game.all_crafts:
                if isinstance(craft.product, (int)) == False:
                    if craft.product[0] in ["1", "2", "3", "4", "5"]:
                        screen.blit(game.all_images[craft.product[0]], (craft.rect.x + 220, craft.rect.y + 15))
                    else:
                        screen.blit(game.all_images[craft.product], (craft.rect.x + 220, craft.rect.y + 15))
                else:
                    i = 220
                    j = 240
                    if craft.rect.x == 415:
                        i = 300
                        j = 335
                    if craft.product < 10:
                        screen.blit(game.font.render(str(craft.product), 1, (100, 20, 20)), (craft.rect.x + i, craft.rect.y - 5))
                        screen.blit(coin_image, (craft.rect.x + j, craft.rect.y + 23))
                    else:
                        screen.blit(game.little_font.render(str(craft.product), 1, (100, 20, 20)), (craft.rect.x + i, craft.rect.y + 12))
                        screen.blit(coin_image, (craft.rect.x + j, craft.rect.y + 23))

                for index, element in enumerate(craft.recipe):
                    i = 25
                    if craft.rect.x == 415:
                        for j in range(3):
                            screen.blit(game.font.render(str(game.boat_trade[j * 2 + 2]), 1, (100, 20, 20)), (450, j * 100 + 215))
                        i = 95
                    screen.blit(game.all_images[element], (craft.rect.x + index * 55 + i, craft.rect.y + 15))

            if game.interface == 1:
                if game.current == 5:
                    screen.blit(age_ribbon, (390, 200))
                    screen.blit(game.font.render(game.text, 1, (100, 20, 20)), (520, 320))
                    screen.blit(game.font.render(game.text2, 1, (100, 20, 20)), (450, 370))
                    screen.blit(game.font.render(game.text3, 1, (100, 20, 20)), ((1280 - len(game.text3) * 19) / 2, 430))

            if game.is_tips:
                game.tips_time += 1
                screen.blit(game.tips_image, (game.tips_co_x, 0))
                screen.blit(game.little_font.render(game.text, 1, (100, 20, 20)), (game.tips_co_x + 20, 10))
                screen.blit(game.little_font.render(game.text2, 1, (100, 20, 20)), (game.tips_co_x + 20, 50))
                screen.blit(game.little_font.render(game.text3, 1, (100, 20, 20)), (game.tips_co_x + 20, 90))

                if game.tips_co_x > 980:
                    game.tips_co_x -= 30

                if game.tips_time >= 140:
                    game.tips_co_x += 60

                    if game.tips_co_x >= 1580:
                        game.reset_tips("", "", "")
                        game.is_tips = False

            if game.age >= 2:
                if game.all_tips["boat"]:
                    screen.blit(game.boat_wave, (981, 488))
                    screen.blit(game.boat_image, game.boat_rect)
                if game.is_tips == False:
                    screen.blit(coin_interface, (1080, 20))
                    screen.blit(coin_image, (1220, 29))
                    screen.blit(game.font.render(str(game.money), 1, (100, 20, 20)), (1090, 5))
                    i = 0
                    for index, potion in enumerate(game.potions):
                        if potion > 0:
                            screen.blit(game.all_images[str(index + 1)], (1228 - i*64, 75))
                            screen.blit(game.min_font.render(f"{math.ceil(potion / 20) // 60} : {math.ceil(math.ceil(potion / 20) % 60)}", 1, (100, 20, 20)), (1228 - i*64, 105))
                            i += 1
            game.all_buttons.draw(screen)
            if game.delete_sell:
                screen.blit(little_coin_image, (125, 35))


        if game.age >= 2:
            if game.age >= 3:
                game.potion_usage()
            if game.age >= 4:
                if game.fusee_time > 0:
                    if game.fusee_rect_y > -200:
                        game.fusee_rect_y -= 3 * game.animal_speed_potion
                        if game.fusee_rect_y < -200:
                            game.fusee_rect_y = -200
                    else:
                        game.fusee_time -= 1 * game.animal_speed_potion
                        if game.fusee_time < 0:
                            game.fusee_time = 0
                elif game.fusee_rect_y < game.fusee_pad_rect_y:
                    game.fusee_rect_y += 3 * game.animal_speed_potion
                    if game.fusee_rect_y > game.fusee_pad_rect_y:
                        game.fusee_rect_y = game.fusee_pad_rect_y
                        for build in game.all_builds:
                            if build.name == "launch_pad_on":
                                build.image = pygame.transform.scale(pygame.image.load(f"assets/builds/launch_pad_on.png"), (100, 100))
                                game.spawn_item("star", build.pos_x, build.pos_y, False)

            if game.all_tips["market"]:
                game.change_market_trade_time += random.randint(1, 2)
            if game.change_market_trade_time >= 700:
                if game.current != 8:
                    game.change_trade_market(False)
            game.animal_time += random.randint(1, 2) * game.animal_speed_potion
            if game.animal_time >= 1600 + len(game.all_animals) * 200:
                game.spawn_animal()
            if len(game.all_pnj) < 2:
                game.pnj_time += random.randint(1, 2) * game.animal_speed_potion
                if game.pnj_time >= 800:
                    game.spawn_pnj()

            if game.boat_rect.x > 1100 and game.boat_rect.x < 1280 and game.boat_time != 0:
                game.boat_rect.x += 2 * game.animal_speed_potion

            if game.boat_time != 0:
                game.boat_time += random.randint(1, 2) * game.animal_speed_potion
                if game.boat_time >= 2400:
                    game.boat_time = 0
                    if game.interface != 2:
                        game.spawn_button("remove", 1240, 650, 30)
                    game.boat_image = pygame.transform.scale(pygame.image.load("assets/builds/boat.png"), (130, 130))

            if game.boat_time == 0 and game.boat_rect.x > 1100:
                game.boat_rect.x -= 2 * game.animal_speed_potion
                if game.boat_rect.x < 1100:
                    game.boat_rect.x = 1100

            for animal in game.all_animals:
                if animal.lock == False:
                    animal.time += random.randint(1, 2) * game.animal_speed_potion
                    if animal.time >= 500 and animal.destination == [0, 0]:
                        game.move_animal(animal)
                if animal.moving:
                    i = 8 * game.animal_speed_potion
                    for k in range(i):
                        animal.rect.x += animal.destination[0]
                        animal.rect.y += animal.destination[1]
                        if animal.rect.x == animal.pos_x * 128 + 205 and animal.destination[0] != 0 or animal.rect.y == animal.pos_y * 128 - 67 and animal.destination[1] != 0:
                            animal.moving = False
                            animal.destination = [0, 0]
                            break

        for build in game.all_builds:
            if build.max_time != None:
                build.produce()

    else:
        if game.current == 0:
            screen.blit(new_game_button, new_game_button_rect)
            screen.blit(saves_button, saves_button_rect)
        elif game.current == 6 or game.current == 7:
            screen.blit(little_interface, (340, 187))
            game.all_buttons.draw(screen)
            if game.current == 6:
                for index, name in enumerate(game.saves_names):
                    screen.blit(game.font.render(name, 1, (100, 20, 20)), (470, index * 100 + 220))
                    with open(f"saves/{name}/infos.txt", "r+") as file:
                        for line in file:
                            screen.blit(game.font.render("Age : " + line, 1, (100, 20, 20)), (780, index * 100 + 220))
                            break
            else:
                screen.blit(game.font.render("Jouer", 1, (100, 20, 20)), (470, 220))
                screen.blit(game.font.render(game.text, 1, (100, 20, 20)), (470, 320))
                screen.blit(game.font.render("Supprimer", 1, (100, 20, 20)), (470, 420))


    pygame.display.flip()
    #
    # la suite du code ne sert qu'à détecter les actions du joueur.
    #
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = False
            game.save_game()
            pygame.quit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            stop = False
            if game.is_running == True:
                if game.interface == 0:

                    for button in game.all_buttons:
                        if button.rect.collidepoint(event.pos):

                            if button.name == "remove":
                                game.delete_button(button)

                            elif button.name == "delete_off":
                                game.delete = True
                                game.all_buttons.remove(button)
                                game.spawn_button("delete", 100, 10, 50)
                                game.reset_tips("Vous pouvez détruire", "les objets en ", "cliquant dessus")
                                game.sound.play("click")

                            elif button.name == "delete":
                                game.delete = False
                                game.all_buttons.remove(button)
                                game.spawn_button("delete_off", 100, 10, 50)
                                game.sound.play("click")

                            elif button.name == "build_helper_button":
                                game.open_build_helper()
                                game.sound.play("click")

                            elif button.name == "arrow_down":
                                game.sound.play("click")
                                if game.craft_offset == 0:
                                    game.spawn_button("arrow_up", 170, 20, 30)
                                game.craft_offset += 1
                                if len(game.actual_craft) - game.craft_offset - 7 == 0:
                                    game.all_buttons.remove(button)
                                game.arrow_button(0)

                            elif button.name == "arrow_up":
                                game.craft_offset -= 1
                                game.sound.play("click")
                                if game.craft_offset == 0:
                                    game.all_buttons.remove(button)
                                if len(game.actual_craft) - 7 - game.craft_offset > 0:
                                    game.spawn_button("arrow_down", 140, 680, 30)
                                game.arrow_button(0)

                            elif button.name == "remove_hopper":
                                game.all_hopper.remove(button.hopper)

                            elif button.name == "rotate":
                                button.hopper.rotate()

                            stop = True
                        if button.name == "remove" or button.name == "remove_hopper" or button.name == "rotate":
                            if button.rect.x != 1180 and button.rect.x != 1240:
                                    game.all_buttons.remove(button)

                    if stop == False:
                        for hopper in game.all_hopper:
                            if hopper.rect.collidepoint(event.pos):
                                game.spawn_button("remove_hopper", hopper.rect.x + 40, hopper.rect.y - 5, 25)
                                game.spawn_button("rotate", hopper.rect.x + 40, hopper.rect.y + 30, 25)
                                for button in game.all_buttons:
                                    if button.rect.x == hopper.rect.x + 40 and button.rect.y == hopper.rect.y - 5:
                                        button.hopper = hopper
                                    elif button.rect.x ==  hopper.rect.x + 40 and button.rect.y == hopper.rect.y + 30:
                                        button.hopper = hopper
                                stop = True

                    if stop == False:

                        for item in game.all_items:
                            if item.rect.collidepoint(event.pos):
                                if game.delete == False or item.name in game.undeletable or item.name[0] in ["1", "2", "3", "4", "5"]:
                                    for itemx in game.all_items:
                                        itemx.can_move = False
                                    item.can_move = True
                                    game.moving_item = [item.pos_x, item.pos_y]
                                    stop = True
                                    break
                                else:
                                    game.remove_item_fast(item)
                                    stop = True
                                    if game.delete_sell:
                                        game.money += game.item_price[item.name]
                                    break

                    if stop == False:
                        for item in game.all_items:
                            if item.can_move == True and stop == False:
                                if 320 < event.pos[0] < 960 and 50 < event.pos[1] < 690:
                                    i = False
                                    if item.name in game.posable_item:
                                        i = True
                                    elif item.name[0] in ["1", "2", "3", "4", "5"]:
                                        if game.potions[int(item.name[0]) - 1] == 0:
                                            i = True
                                    elif item.name == "ticket":
                                        i = True
                                    stop = True
                                    if i:
                                        if game.build_item(item, event.pos) == False:
                                            if game.move_item(item, event.pos, True) != True:
                                                stop = False
                                    elif item.name == "hopper":
                                        if game.build_hopper(event.pos, item) != True:
                                            if game.move_item(item, event.pos, True) != True:
                                                stop = False
                                    elif item.name == "axe" or item.name == "pickaxe" or item.name == "shovel":
                                        if game.affect_item(item, event.pos) == False:
                                            if item.affected == True:
                                                if game.unaffect_item(item, item.pos_x, item.pos_y, event.pos[0], event.pos[1], False) != True:
                                                    if game.move_item(item, event.pos, True) != True:
                                                        stop = False
                                            else:
                                                if game.move_item(item, event.pos, True) != True:
                                                    stop = False
                                    else:
                                        if game.move_item(item, event.pos, True) != True:
                                            stop = False
                                    break

                    if stop == False:
                        for build in game.all_builds:
                            if 320 < event.pos[0] < 960 and 50 < event.pos[1] < 690:
                                if build.rect.collidepoint(event.pos):
                                    if build.name == "chest" or build.name == "treasure":
                                        game.open_chest(build)
                                        game.sound.play("chest")
                                    elif build.name == "big_craft_table":
                                        game.open_big_craft_table()
                                        game.sound.play("click")
                                    elif build.name == "market":
                                        game.open_market()
                                        game.sound.play("click")
                                    elif build.name == "alembic":
                                        game.open_alembic()
                                        game.sound.play("click")
                                    elif build.name == "kitchen":
                                        game.open_kitchen()
                                        game.sound.play("click")
                                    elif build.name == "meule":
                                        game.open_meule()
                                        game.sound.play("click")
                                    elif build.name == "building":
                                        game.open_building()
                                        game.sound.play("click")
                                    else:
                                        game.spawn_button("remove", build.rect.x + 81, build.rect.y - 13, 25)
                                    stop = True
                                    break

                    if stop == False:
                        for animal in game.all_animals:
                            if animal.rect.collidepoint(event.pos):
                                if animal.moving == False:
                                    game.spawn_button("remove", animal.rect.x + 81, animal.rect.y - 13, 25)
                                stop = True
                                break

                    if stop == False:
                        for craft in game.all_crafts:
                            if craft.rect.collidepoint(event.pos):
                                if craft.rect.x == 15:
                                    game.craft_item(craft)
                                else:
                                    game.crafting(craft)
                                stop = True
                                break

                    if stop == False:
                        if game.all_tips["boat"]:
                            if game.boat_rect.collidepoint(event.pos):
                                if game.boat_rect.x == 1100:
                                    game.open_boat()
                                    game.sound.play("click")
                                    stop = True

                else:
                    for button in game.all_buttons:
                        if button.rect.collidepoint(event.pos):
                            if button.name == "cross":
                                game.cross()
                                game.sound.play("click")
                                stop = True
                                break
                            elif game.interface == 2 and button.name in ["arrow_up", "arrow_down"]:
                                if game.all_select_builder[game.selected_builder - 1] == "build_helper_button" and game.current == 4:
                                    i = 3 #le nombre de craft qu'on décale en appuyant sur la flèche
                                    j = 0 # l'id de l'interface pour game.arrow_button
                                    k = 18 # le nombre de craft sur l'écran
                                else:
                                    if game.current == 4:
                                        j = game.selected_builder
                                        if game.selected_builder == 2 or game.selected_builder == 4:
                                            k = 6
                                    elif game.current == 10 or game.current == 3:
                                        j = 10
                                        k = 7
                                    elif game.current == 15:
                                        j = 12
                                        k = 6
                                    i = 1
                                if button.name == "arrow_down":
                                    game.sound.play("click")
                                    game.craft_offset += i
                                    game.arrow_button(j)
                                    game.spawn_button("arrow_up", 1220, 305, 50)
                                    if len(game.actual_craft) - game.craft_offset - k <= 0:
                                        game.all_buttons.remove(button)
                                    stop = True

                                elif button.name == "arrow_up":
                                    game.sound.play("click")
                                    game.craft_offset -= i
                                    game.arrow_button(j)
                                    game.spawn_button("arrow_down", 1220, 385, 50)
                                    if game.craft_offset <= 0:
                                        game.all_buttons.remove(button)
                                    stop = True

                    if stop == False:

                        if game.current == 2:
                            for item in game.all_interface_item:
                                if item.rect.collidepoint(event.pos):
                                    game.remove_form_chest(item)
                                    break

                        elif game.current == 4:
                            for button in game.all_interface_item:
                                if button.rect.collidepoint(event.pos):
                                    if button.name in game.all_select_builder:
                                        game.selected_builder = game.all_select_builder.index(button.name) + 1
                                        game.open_build_helper()
                                        game.sound.play("click")
                                        break

                        elif game.current == 8:
                            for button in game.all_buttons:
                                if button.rect.collidepoint(event.pos):
                                    if button.name == "market_trade":
                                        game.buy_item(button)
                                        break

                        elif game.current == 9:
                            for item in game.all_interface_item:
                                if item.rect.collidepoint(event.pos):
                                    game.remove_from_treasure(item)
                                    break

                        elif game.current == 10:
                            for build in game.all_builds:
                                if build.name == "kitchen":
                                    if build.fuel > 0:
                                        for craft in game.all_crafts:
                                            if craft.rect.collidepoint(event.pos):
                                                build.fuel -= 1
                                                game.crafting(craft)
                                                break
                                        break

                        elif game.current == 11:
                            for craft in game.all_crafts:
                                if craft.rect.collidepoint(event.pos):
                                    game.boat_trading(craft)

                        elif game.current == 12:
                            for craft in game.all_crafts:
                                if craft.rect.collidepoint(event.pos):
                                    game.sound.play("click")
                                    game.click_on_potion(craft)

                        elif game.current == 13 or game.current == 14 or game.current == 3:
                            for craft in game.all_crafts:
                                if craft.rect.collidepoint(event.pos):
                                    game.crafting(craft)
                                    break

                        elif game.current == 15:
                            for button in game.all_interface_item:
                                if button.rect.collidepoint(event.pos):
                                    if button.name in game.building:
                                        game.selected_builder = game.building.index(button.name) + 1
                                        game.see_building_crafts()
                                        game.sound.play("click")
                                        break

                            if game.building[game.selected_builder - 1] == "market":
                                for buttonx in game.all_buttons:
                                    if buttonx.rect.collidepoint(event.pos):
                                        if buttonx.name == "market_trade":
                                            game.buy_item(buttonx)
                                            break

                            if game.building[game.selected_builder - 1] == "pass":
                                i = 0
                                for button in game.all_buttons:
                                    if button.name == "market_trade":
                                        i += 1
                                        if button.rect.collidepoint(event.pos) and i == game.pass_number:
                                            game.buy_pass(button)
                                            break

                            for craft in game.all_crafts:
                                if craft.rect.collidepoint(event.pos):
                                    if game.building[game.selected_builder - 1] == "alembic":
                                        game.click_on_potion(craft)
                                        game.sound.play("click")
                                        break
                                    elif game.building[game.selected_builder - 1] == "kitchen":
                                        for build in game.all_builds:
                                            if build.name == "building":
                                                if build.fuel > 0:
                                                    build.fuel -= 1
                                                    game.crafting(craft)
                                                break
                                    else:
                                        game.crafting(craft)
                                        break

            else:
                if new_game_button_rect.collidepoint(event.pos) and game.interface == 0:
                    game.new_game()
                    game.sound.play("click")

                elif saves_button_rect.collidepoint(event.pos):
                    game.current = 6
                    game.interface = 1
                    game.update_cross_pos()
                    game.spawn_save()
                    game.sound.play("click")

                if game.interface != 0:
                    for button in game.all_buttons:
                        if button.rect.collidepoint(event.pos):
                            game.sound.play("click")
                            if button.name == "cross":
                                game.cross()
                            elif button.name == "save1" or button.name == "save2" or button.name == "save3":
                                game.selected_save = int(button.name[4])
                                game.current = 7
                                game.spawn_buttons_save()

                            elif button.name == "play":
                                game.load_game()
                                game.is_running = True
                                game.interface = 0
                                game.current = 1

                            elif button.name == "delete":
                                game.interface = 0
                                game.current = 0
                                shutil.rmtree(f"saves/{game.saves_names[game.selected_save - 1]}")
                                game.saves_names.remove(game.saves_names[game.selected_save - 1])
                                game.selected_save = 0

                            elif button.name == "rename":
                                game.writing = True
                                game.text = ""
                                game.spawn_button("checked", 840, 330, 50)

                            elif button.name == "checked":
                                if game.text != "":
                                    if game.text not in game.saves_names:
                                        game.writing = False
                                        game.all_buttons.remove(button)
                                        os.rename(f"saves/{game.saves_names[game.selected_save - 1]}", f"saves/{game.text}")
                                        game.saves_names = os.listdir("saves")

                            break

        elif event.type == pygame.KEYDOWN:
            if game.writing:
                if 97 <= event.key <= 122 and len(game.text) < 18:
                    game.text += pygame.key.name(event.key)
                elif event.key == 8 and game.text != "":
                    game.text = game.text[:len(game.text) - 1]


    clock.tick(FPS)
