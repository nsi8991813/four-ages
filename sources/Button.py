import pygame


class InterfaceItem(pygame.sprite.Sprite):
    def __init__(self, co_x, co_y, name, scale):
        super().__init__()

        self.name = name

        self.image = pygame.image.load(f"assets/items/{name}.png")
        self.image = pygame.transform.scale(self.image, (scale, scale))
        self.image = pygame.transform.rotozoom(self.image, 90, 1)
        self.rect = self.image.get_rect()
        self.rect.x = co_x
        self.rect.y = co_y
