import pygame


class InterfaceItem(pygame.sprite.Sprite):
    def __init__(self, game, co_x, co_y, name, scale):
        super().__init__()
        self.game = game

        self.name = name

        if self.name == "cross" or self.name == "save1" or self.name == "save2" or self.name == "save3" or self.name == "delete" or self.name == "play" or self.name == "rename" or self.name == "checked" or self.name == "remove" or self.name == "market_trade" or self.name == "arrow_up" or self.name == "arrow_down" or self.name == "rotate" or self.name == "remove_hopper":
            self.image = pygame.image.load(f"assets/{name}.png")
        elif self.name not in self.game.posable_item:
            self.image = pygame.image.load(f"assets/items/{self.name}.png")
        else:
            self.image = pygame.image.load(f"assets/builds/{self.name}.png")
        self.image = pygame.transform.scale(self.image, (scale, scale))
        self.rect = self.image.get_rect()
        self.rect.x = co_x
        self.rect.y = co_y
        if self.game.potions[3] >= 1:
            self.game.rotate(self)

        if self.name == "market_trade":
            self.price = 0
            self.item = 0
            self.scale = scale

        if self.name == "remove_hopper":
            self.hopper = 0
