import pygame


class Hopper(pygame.sprite.Sprite):

    def __init__(self, game, pos_x, pos_y, direction):
        super().__init__()

        self.game = game

        self.pos_x = pos_x
        self.pos_y = pos_y
        self.image = pygame.image.load("assets/items/hopper.png")
        self.image = pygame.transform.scale(self.image, (50, 50))
        self.origin_image = self.image
        self.rect = self.image.get_rect()
        self.rect.x = self.pos_x * 128 + 231
        self.rect.y = self.pos_y * 128 - 115
        self.direction = direction
        if self.direction == [0, -1]:
            self.direction = [-1, 0]

        elif self.direction == [1, 0]:
            self.direction = [0, -1]

        elif self.direction == [0, 1]:
            self.direction = [1, 0]

        elif self.direction == [-1, 0]:
            self.direction = [0, 1]
        self.rotate()

    def rotate(self):
        self.rect = self.image.get_rect(center=self.rect.center)

        if self.direction == [- 1, 0]:
            self.direction = [0, -1]
            self.rect.x = self.pos_x * 128 + 231
            self.rect.y = self.pos_y * 128 - 115
            self.image = pygame.transform.rotozoom(self.origin_image, -90, 1)
            self.hopper_verification()

        elif self.direction == [0, -1]:
            self.direction = [1, 0]
            self.rect.x = self.pos_x * 128 + 295
            self.rect.y = self.pos_y * 128 - 50
            self.image = pygame.transform.rotozoom(self.origin_image, 180, 1)
            self.hopper_verification()

        elif self.direction == [1, 0]:
            self.direction = [0, 1]
            self.rect.x = self.pos_x * 128 + 231
            self.rect.y = self.pos_y * 128 + 13
            self.image = pygame.transform.rotozoom(self.origin_image, 90, 1)
            self.hopper_verification()

        elif self.direction == [0, 1]:
            self.direction = [-1, 0]
            self.rect.x = self.pos_x * 128 + 167
            self.rect.y = self.pos_y * 128 - 50
            self.image = self.origin_image
            self.hopper_verification()

    def hopper_verification(self):
        for hopper in self.game.all_hopper:
            if hopper.pos_x == self.pos_x + self.direction[0] and hopper.pos_y == self.pos_y + self.direction[1]:
                if hopper.direction[0] + hopper.pos_x == self.pos_x and hopper.direction[1] + hopper.pos_y == self.pos_y:
                    self.rotate()
