import pygame


class ChestItem(pygame.sprite.Sprite):
    def __init__(self, build, co_x, co_y, name, litte):
        super().__init__()
        self.build = build

        self.name = name

        if self.name not in self.build.game.posable_item:
            self.image = pygame.image.load(f"assets/items/{name}.png")
        else:
            self.image = pygame.image.load(f"assets/builds/{name}.png")
        if litte == True:
            self.image = pygame.transform.scale(self.image, (40, 40))
        else:
            self.image = pygame.transform.scale(self.image, (100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = co_x
        self.rect.y = co_y
        if self.build.game.potions[3] >= 1:
            self.build.game.rotate(self)