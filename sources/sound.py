import pygame
import pygame.mixer


class Sound:

    def __init__(self):
        self.sounds = {
            "click": pygame.mixer.Sound("assets/sounds/click.MP3"),
            "buy": pygame.mixer.Sound("assets/sounds/buy.MP3"),
            "beef": pygame.mixer.Sound("assets/sounds/beef.MP3"),
            "bonus": pygame.mixer.Sound("assets/sounds/bonus.MP3"),
            "chest": pygame.mixer.Sound("assets/sounds/chest.MP3"),
            "clou": pygame.mixer.Sound("assets/sounds/clou.MP3"),
            "hache": pygame.mixer.Sound("assets/sounds/hache.MP3"),
            "potion_craft": pygame.mixer.Sound("assets/sounds/potion_craft.MP3"),
            "lamb": pygame.mixer.Sound("assets/sounds/lamb.MP3"),
            "chicken": pygame.mixer.Sound("assets/sounds/chicken.MP3"),
            "fusée": pygame.mixer.Sound("assets/sounds/fusee.MP3"),
            "music": pygame.mixer.Sound("assets/sounds/music.MP3")
        }

    def play(self, name):
        if name != "music":
            self.sounds[name].play()
        else:
            self.sounds["music"].play(loops=-1)
